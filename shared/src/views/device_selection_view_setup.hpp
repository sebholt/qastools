/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QKeySequence>
#include <QString>

namespace Views
{

/// @brief Device_Selection_View_Setup
///
class Device_Selection_View_Setup
{
  public:
  // -- Construction

  Device_Selection_View_Setup ();

  ~Device_Selection_View_Setup ();

  public:
  // -- Attributes
  QKeySequence kseq_toggle_vis = Qt::Key_F6;
  QString user_device;
};

} // namespace Views
