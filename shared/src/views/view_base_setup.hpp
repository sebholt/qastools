/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

namespace Views
{

/// @brief View_Base general static setup variables
///
class View_Base_Setup
{
  public:
  // -- Construction

  View_Base_Setup ();

  virtual ~View_Base_Setup ();

  public:
  // -- Attributes
};

} // namespace Views
