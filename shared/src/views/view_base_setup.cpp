/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "view_base_setup.hpp"

namespace Views
{

View_Base_Setup::View_Base_Setup () = default;

View_Base_Setup::~View_Base_Setup () = default;

} // namespace Views
