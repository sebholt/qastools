/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QStackedLayout>
#include <QWidget>

// Forward declaration
namespace MWdg
{
class Mixer_Device_Setup;
class Inputs_Setup;
} // namespace MWdg
namespace Views
{
class Message_Widget;
class View_Base_Setup;
} // namespace Views

namespace Views
{

/// @brief View_Base
///
class View_Base : public QWidget
{
  Q_OBJECT

  public:
  // -- Construction

  View_Base ( QWidget * parent_n = nullptr );

  virtual ~View_Base ();

  // -- Layout and message widget

  /// The stacked layout holds the actual view widget and a
  /// (most times) hidden message widget that can be used for
  /// displaying error or info texts.
  QStackedLayout *
  lay_stack () const
  {
    return _lay_stack;
  }

  /// @brief The message widget is mostly used for error messages
  ///
  Message_Widget *
  message_wdg ()
  {
    return _message_wdg;
  }

  // -- Mixer device setup

  /// @brief Mixer device setup
  ///
  /// @return The setup
  const MWdg::Mixer_Device_Setup *
  mixer_dev_setup ()
  {
    return _mixer_dev_setup;
  }

  virtual void
  set_mixer_dev_setup ( const MWdg::Mixer_Device_Setup * setup_n );

  // -- Input setup (keyboard/mouse)

  /// @brief Inputs (keyboard/mouse) setup
  ///
  /// @return The setup
  const MWdg::Inputs_Setup *
  inputs_setup ()
  {
    return _inputs_setup;
  }

  virtual void
  set_inputs_setup ( const MWdg::Inputs_Setup * setup_n );

  // -- View setup (colors, element selection, etc.)

  virtual void
  set_view_setup ( Views::View_Base_Setup * setup_n ) = 0;

  // -- Signals

  Q_SIGNAL
  void
  sig_mixer_dev_reload_request ();

  private:
  // -- Attributes
  const MWdg::Mixer_Device_Setup * _mixer_dev_setup = nullptr;
  const MWdg::Inputs_Setup * _inputs_setup = nullptr;
  Views::Message_Widget * _message_wdg = nullptr;
  QStackedLayout * _lay_stack = nullptr;
};

} // namespace Views
