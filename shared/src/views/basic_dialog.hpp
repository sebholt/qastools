/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QDialog>
#include <QLabel>
#include <QPushButton>
#include <QVBoxLayout>

namespace Views
{

/// @brief Basic_Dialog
///
class Basic_Dialog : public QDialog
{
  Q_OBJECT

  public:
  // -- Construction

  Basic_Dialog ( QWidget * parent_n = nullptr,
                 Qt::WindowFlags flags_n = Qt::WindowFlags () );

  ~Basic_Dialog ();

  // -- Setup

  void
  set_title_str ( const QString & str_n );

  void
  set_central_widget ( QWidget * wdg_n );

  protected:
  // -- Utility

  /// @brief Creates a close button
  QPushButton *
  create_close_button ();

  private:
  // -- Attributes
  // Pages
  QLabel * _lbl_title = nullptr;
  QVBoxLayout * _lay_main = nullptr;
};

} // namespace Views
