/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QObject>
#include <QPicture>
#include <memory>
#include <vector>

// Forward declaration
namespace Wdg::Pad_Proxy
{
class Group;
} // namespace Wdg::Pad_Proxy
namespace Wdg::Sliders_Pad
{
class Data;
class Data_Column;
} // namespace Wdg::Sliders_Pad

namespace Wdg::Sliders_Pad
{

/// @brief Data_Group
///
class Data_Group : public QObject
{
  Q_OBJECT

  public:
  // -- Construction

  Data_Group ( QObject * parent_n = nullptr );

  ~Data_Group ();

  // -- Parent

  Data *
  sp_data ();

  // -- Columns

  std::size_t
  num_columns () const
  {
    return _columns.size ();
  }

  Data_Column *
  column ( std::size_t index_n ) const
  {
    return _columns[ index_n ].get ();
  }

  void
  reserve_columns ( std::size_t size_n );

  void
  append_column ( Data_Column * column_n );

  // -- Public attributes
  unsigned int group_idx = 0;
  unsigned int group_pos = 0;
  unsigned int group_width = 0;
  unsigned int num_sliders = 0;
  unsigned int num_switches = 0;

  QPicture center_pic;   // Center picture
  QPicture hd_pics[ 2 ]; // Header/footer pictures
  Wdg::Pad_Proxy::Group * sppg = nullptr;

  private:
  // -- Private attributes
  std::vector< std::unique_ptr< Data_Column > > _columns;
};

} // namespace Wdg::Sliders_Pad
