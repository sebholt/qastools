/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "data.hpp"
#include "wdg/sliders_pad/data_group.hpp"
#include "wdg/sliders_pad/footer.hpp"
#include "wdg/sliders_pad/header.hpp"
#include <iostream>

namespace Wdg::Sliders_Pad
{

Data::Data ( QObject * parent_n )
: QObject ( parent_n )
{
}

Data::~Data () = default;

Header *
Data::header_cast ()
{
  return static_cast< Header * > ( header );
}

Footer *
Data::footer_cast ()
{
  return static_cast< Footer * > ( footer );
}

void
Data::reserve_groups ( std::size_t size_n )
{
  _groups.reserve ( size_n );
}

void
Data::append_group ( Data_Group * group_n )
{
  _groups.emplace_back ( group_n );
}

void
Data::clear_groups ()
{
  _groups.clear ();
}

} // namespace Wdg::Sliders_Pad
