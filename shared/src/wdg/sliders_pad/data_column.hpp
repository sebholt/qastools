/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QObject>
#include <QPicture>

// Forward declaration
namespace Wdg::Pad_Proxy
{
class Column;
} // namespace Wdg::Pad_Proxy
namespace Wdg::Sliders_Pad
{
class Data;
class Data_Group;
} // namespace Wdg::Sliders_Pad

namespace Wdg::Sliders_Pad
{

/// @brief Data_Column
///
class Data_Column : public QObject
{
  Q_OBJECT

  public:
  // -- Construction

  Data_Column ( QObject * parent_n = nullptr );

  ~Data_Column ();

  // -- Parent

  Data_Group *
  sp_group ();

  Data *
  sp_data ();

  // -- Public slots

  Q_SLOT
  void
  update_footer_label ();

  // -- Attributes
  unsigned int col_idx = 0;
  unsigned int col_total_idx = 0;

  unsigned int col_pos = 0;
  unsigned int col_width = 0;

  QPicture hd_pics[ 2 ]; // Header/footer pictures

  bool show_value_label = true;
  Wdg::Pad_Proxy::Column * sppc = nullptr;
};

} // namespace Wdg::Sliders_Pad
