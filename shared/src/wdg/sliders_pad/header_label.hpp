/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QColor>
#include <QRect>
#include <QRectF>
#include <QString>
#include <QTransform>

namespace Wdg::Sliders_Pad
{

/// @brief Header_Label
///
class Header_Label
{
  public:
  // -- Construction

  Header_Label ();

  ~Header_Label ();

  // -- Attributes
  unsigned int label_length_max = 0;
  QTransform label_trans;
  QTransform label_trans_inv;
  QRectF label_rect;
  QRectF text_area;
  QRectF text_rect;
  QRect label_txt_bbox;

  unsigned int group_idx = 0;
  unsigned int column_idx = 0;
  unsigned int column_total_idx = 0;

  QString text;
  QString text_elided;
  QString tool_tip;

  QColor col_fg;
};

} // namespace Wdg::Sliders_Pad
