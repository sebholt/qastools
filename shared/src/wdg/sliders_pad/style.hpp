/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QPainter>
#include <QPen>
#include <QPicture>

namespace Wdg::Sliders_Pad
{

// Forward declaration
class Data;
class Data_Group;
class Data_Column;

/// @brief Style
///
class Style
{
  public:
  // -- Construction

  Style ( Data * data_n );

  virtual ~Style ();

  // -- Data

  Data *
  sp_data () const
  {
    return _sp_data;
  }

  // -- Painting

  void
  paint_base_decoration ();

  void
  paint_header_decoration ();

  void
  paint_footer_decoration ();

  protected:
  // -- Protected methods

  // Utility

  double
  calc_col_center ( unsigned int col_width_n,
                    unsigned int col_idx_n,
                    unsigned int num_cols_n ) const;

  QPainterPath
  bridge_path ( Data_Group * sp_grp_n, double y_top_n ) const;

  QPainterPath
  inclined_bridge_path ( Data_Group * sp_grp_n, double y_top_n ) const;

  QPainterPath
  inclined_multi_bridge_path ( Data_Group * sp_grp_n, double y_top_n ) const;

  // Base drawing

  virtual void
  draw_base ();

  void
  dbase_sliders_bridge ( QPainter & pnt_n, Data_Group * sp_grp_n );

  void
  dbase_switches_bridge ( QPainter & pnt_n, Data_Group * sp_grp_n );

  void
  dbase_single_slider_stem ( QPainter & pnt_n, Data_Group * sp_grp_n );

  void
  dbase_single_switch_stem ( QPainter & pnt_n, Data_Group * sp_grp_n );

  void
  dbase_multi_switch_stems ( QPainter & pnt_n, Data_Group * sp_grp_n );

  void
  dbase_labels_connectors ( QPainter & pnt_n, Data_Group * sp_grp_n );

  // Header drawing

  virtual void
  draw_header ();

  void
  dheader_stem ( QPainter & pnt_n, Data_Group * sp_grp_n );

  void
  dheader_bridge ( QPainter & pnt_n, Data_Group * sp_grp_n );

  // Footer drawing

  virtual void
  draw_footer ();

  void
  dfooter_stem ( QPainter & pnt_n,
                 Data_Group * sp_grp_n,
                 Data_Column * sp_col_n );

  public:
  // -- Attributes
  QPen stem_pen;
  double stem_corner_indent = 1.0;

  private:
  // -- Attributes
  Data * _sp_data = nullptr;

  // Painting variables
  unsigned int _area_left = 0;
  unsigned int _area_height = 0;
  unsigned int _y_top = 0;
  unsigned int _y_bottom = 0;
  double _x_mid = 0.0;
  double _y_mid = 0.0;
};

} // namespace Wdg::Sliders_Pad
