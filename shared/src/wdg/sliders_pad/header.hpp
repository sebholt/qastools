/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "wdg/sliders_pad/header_data.hpp"
#include <QBrush>
#include <QFont>
#include <QPen>
#include <QWidget>

namespace Wdg::Sliders_Pad
{

// Forward declaration
class Data;
class Data_Group;
class Data_Column;
class Style;

/// @brief Header
///
class Header : public QWidget
{
  Q_OBJECT

  public:
  // -- Construction

  Header ( Data * sp_data_n, Style * sp_style_n, QWidget * parent_n = nullptr );

  ~Header ();

  // -- Size hints

  QSize
  minimumSizeHint () const override;

  QSize
  sizeHint () const override;

  // -- Header variables

  const Header_Data &
  hd_data () const
  {
    return _data;
  }

  Header_Data &
  hd_data ()
  {
    return _data;
  }

  Data *
  sp_data () const
  {
    return _sp_data;
  }

  Style *
  sp_style () const
  {
    return _sp_style;
  }

  // -- Decoration graphics

  /// @brief Sets the index of the label/group with the focus
  void
  set_focus_idx ( unsigned int group_idx_n, unsigned int column_idx_n );

  unsigned int
  label_str_length_px_max ( const QString & str_n ) const;

  // -- Public slots

  Q_SLOT
  void
  set_label_text ( unsigned int lbl_idx_n, const QString & txt_n );

  // -- Signals

  Q_SIGNAL
  void
  sig_label_selected ( unsigned int group_idx_n, unsigned int column_idx_n );

  protected:
  // -- Protected methods

  void
  enterEvent ( QEnterEvent * event_n ) override;

  void
  leaveEvent ( QEvent * event_n ) override;

  void
  mousePressEvent ( QMouseEvent * event_n ) override;

  void
  mouseMoveEvent ( QMouseEvent * event_n ) override;

  void
  changeEvent ( QEvent * event_n ) override;

  void
  paintEvent ( QPaintEvent * event_n ) override;

  void
  paint_label_rects ( QPainter & pnt_n );

  void
  paint_label_decos ( QPainter & pnt_n );

  void
  paint_label_texts ( QPainter & pnt_n );

  void
  paint_label_text ( QPainter & pnt_n,
                     Qt::Alignment txt_align_n,
                     unsigned int lbl_idx_n,
                     unsigned int state_idx_n );

  const Header_Label *
  find_label ( const QPoint & pos_n );

  void
  elided_label_text ( Header_Label & lbl_n );

  void
  update_elided_texts ();

  void
  update_painter_states ();

  private:
  // -- Attributes
  Header_Data _data;
  Data * _sp_data;
  Style * _sp_style;

  unsigned int _focus_idx;
  unsigned int _weak_focus_idx;
  const unsigned int _invalid_idx;

  // 0 - default
  // 1 - focus only
  // 2 - weak focus only
  // 3 - focus and weak focus
  QBrush _lbl_rect_brush[ 3 ];
  QPen _lbl_txt_pen[ 4 ];
  QFont _lbl_txt_font[ 4 ];
};

} // namespace Wdg::Sliders_Pad
