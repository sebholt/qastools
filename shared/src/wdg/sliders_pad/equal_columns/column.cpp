/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "column.hpp"
#include "wdg/sliders_pad/equal_columns/row.hpp"

namespace Wdg::Sliders_Pad::Equal_Columns
{

Column::Column ( unsigned int column_index_n )
: _column_index ( column_index_n )
{
}

Column::~Column () = default;

void
Column::set_column_pos ( unsigned int pos_n )
{
  _column_pos = pos_n;
}

void
Column::set_column_width ( unsigned int width_n )
{
  _column_width = width_n;
}

void
Column::append_row ( Row * item_n )
{
  _rows.emplace_back ( item_n );
}

void
Column::remove_empty_rows_at_back ()
{
  std::size_t idx = num_rows ();
  while ( idx > 0 ) {
    --idx;
    bool remove = true;
    const Row * row = _rows[ idx ].get ();
    if ( row != nullptr ) {
      if ( row->item () != nullptr ) {
        remove = false;
      }
    }
    if ( remove ) {
      _rows.pop_back ();
    } else {
      break;
    }
  }
}

} // namespace Wdg::Sliders_Pad::Equal_Columns
