/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "group.hpp"
#include "wdg/sliders_pad/equal_columns/column.hpp"

namespace Wdg::Sliders_Pad::Equal_Columns
{

Group::Group ( unsigned int group_idx_n )
: _group_index ( group_idx_n )
{
}

Group::~Group () = default;

void
Group::set_group_pos ( unsigned int pos_n )
{
  _group_pos = pos_n;
}

void
Group::set_group_width ( unsigned int width_n )
{
  _group_width = width_n;
}

void
Group::append_column ( Column * column_n )
{
  if ( column_n != nullptr ) {
    _columns.emplace_back ( column_n );
  }
}

void
Group::remove_empty_columns_at_back ()
{
  std::size_t idx = num_columns ();
  while ( idx > 0 ) {
    --idx;
    bool remove = true;
    Column * col = _columns[ idx ].get ();
    if ( col != nullptr ) {
      if ( col->num_rows () != 0 ) {
        remove = false;
      }
    }
    if ( remove ) {
      _columns.pop_back ();
    } else {
      break;
    }
  }
}

} // namespace Wdg::Sliders_Pad::Equal_Columns
