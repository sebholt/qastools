/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <memory>
#include <vector>

namespace Wdg::Sliders_Pad::Equal_Columns
{

// Forward declaration
class Column;

/// @brief Group
///
class Group
{
  public:
  // -- Construction

  Group ( unsigned int group_idx_n );

  virtual ~Group ();

  // -- Group index

  unsigned int
  group_index () const
  {
    return _group_index;
  }

  void
  set_group_index ( unsigned int idx_n )
  {
    _group_index = idx_n;
  }

  // -- Active columns

  unsigned int
  active_columns () const
  {
    return _active_columns;
  }

  void
  set_active_columns ( unsigned int num_n )
  {
    _active_columns = num_n;
  }

  // -- Group pos

  unsigned int
  group_pos () const
  {
    return _group_pos;
  }

  void
  set_group_pos ( unsigned int pos_n );

  // -- Group width

  unsigned int
  group_width () const
  {
    return _group_width;
  }

  void
  set_group_width ( unsigned int pos_n );

  // -- Columns

  void
  append_column ( Column * column_n );

  void
  remove_empty_columns_at_back ();

  std::size_t
  num_columns () const
  {
    return _columns.size ();
  }

  const Column *
  column ( std::size_t idx_n ) const
  {
    return _columns[ idx_n ].get ();
  }

  Column *
  column ( std::size_t idx_n )
  {
    return _columns[ idx_n ].get ();
  }

  // -- Row statistics

  std::vector< unsigned int > &
  row_stats ()
  {
    return _row_stats;
  }

  std::size_t
  num_rows () const
  {
    return _row_stats.size ();
  }

  unsigned int
  row_stat ( std::size_t idx_n ) const
  {
    return _row_stats[ idx_n ];
  }

  private:
  // -- Attributes
  unsigned int _group_index = 0;
  unsigned int _active_columns = 0;
  unsigned int _group_pos = 0;
  unsigned int _group_width = 0;
  std::vector< std::unique_ptr< Column > > _columns;
  std::vector< unsigned int > _row_stats;
};

} // namespace Wdg::Sliders_Pad::Equal_Columns
