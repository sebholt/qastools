/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "index.hpp"

namespace Wdg::Sliders_Pad::Equal_Columns
{

Index::Index () = default;

Index::Index ( Index && index_n ) = default;

Index::~Index () = default;

Index &
Index::operator= ( Index && index_n ) = default;

} // namespace Wdg::Sliders_Pad::Equal_Columns
