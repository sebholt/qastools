/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QLayoutItem>

namespace Wdg::Sliders_Pad::Equal_Columns
{

/// @brief Row
///
class Row
{
  public:
  // -- Construction

  Row ( unsigned int row_idx_n );

  ~Row ();

  // -- Row index

  unsigned int
  row_index () const
  {
    return _row_index;
  }

  void
  set_row_index ( unsigned int row_idx_n )
  {
    _row_index = row_idx_n;
  }

  // -- Layout item

  QLayoutItem *
  item () const
  {
    return _item;
  }

  void
  set_item ( QLayoutItem * item_n )
  {
    _item = item_n;
  }

  private:
  // -- Attributes
  unsigned int _row_index = 0;
  QLayoutItem * _item = nullptr;
};

} // namespace Wdg::Sliders_Pad::Equal_Columns
