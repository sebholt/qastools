/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QLayout>
#include <QObject>
#include <QWidget>
#include <memory>
#include <vector>

namespace Wdg::Sliders_Pad::Equal_Columns
{

// Forward declaration
class Column;
class Group;
class Index;
class Row;

/// @brief Layout
///
class Layout : public QLayout
{
  public:
  // -- Construction

  Layout ( QWidget * parent_n = nullptr );

  ~Layout ();

  // -- Size hints

  QSize
  minimumSize () const override;

  QSize
  sizeHint () const override;

  // -- Variable reader

  std::size_t
  num_groups () const
  {
    return _groups.size ();
  }

  const Group *
  group ( std::size_t idx_n ) const
  {
    return _groups[ idx_n ].get ();
  }

  std::size_t
  num_active_groups () const
  {
    return _groups_active.size ();
  }

  const Group *
  active_group ( std::size_t idx_n ) const
  {
    return _groups_active[ idx_n ];
  }

  int
  spacing_vertical () const
  {
    return _spacing_vertical;
  }

  unsigned int
  smallest_group_dist () const
  {
    return _smallest_group_dist;
  }

  unsigned int
  smallest_column_dist () const
  {
    return _smallest_column_dist;
  }

  std::size_t
  num_rows () const
  {
    return _row_heights.size ();
  }

  unsigned int
  row_min_height ( std::size_t idx_n ) const
  {
    return _row_min_heights[ idx_n ];
  }

  unsigned int
  row_height ( std::size_t idx_n ) const
  {
    return _row_heights[ idx_n ];
  }

  unsigned int
  all_rows_height () const;

  unsigned int
  row_pos ( std::size_t idx_n ) const
  {
    unsigned int res = 0;
    if ( idx_n > 0 ) {
      for ( std::size_t ii = 0; ii <= idx_n; ++ii ) {
        res += _row_heights[ ii ];
        res += spacing_vertical ();
      }
    }
    return res;
  }

  unsigned int
  col_type_width ( std::size_t type_n )
  {
    return _col_widths[ type_n ];
  }

  // -- QLayout methods

  int
  add_group_item ( QLayoutItem * item_n,
                   unsigned int group_idx_n,
                   unsigned int column_idx_n,
                   unsigned int row_idx_n );

  int
  add_group_widget ( QWidget * wdg_n,
                     unsigned int group_idx,
                     unsigned int column_idx,
                     unsigned int row_idx_n );

  void
  addItem ( QLayoutItem * item_n );

  QLayoutItem *
  itemAt ( int index_n ) const;

  QLayoutItem *
  takeAt ( int index_n );

  int
  count () const;

  void
  invalidate ();

  void
  setGeometry ( const QRect & rect_n );

  // Implementation methods
  // Direct use is not recommended

  void
  update_cache_const () const;

  void
  update_cache ();

  /// update_cache should be called before this once
  void
  calc_columns_sizes ( unsigned int area_width_n, unsigned int area_height_n );

  void
  calc_row_heights ( unsigned int height_n );

  void
  calc_column_widths ( unsigned int width_n );

  void
  set_row_height ( unsigned int row_idx_n, unsigned int height_n );

  void
  set_geometries ( const QRect & crect_n );

  void
  set_geometry_row ( const QRect & row_rect_n,
                     const Group * grp_n,
                     const Column * col_n,
                     const Row * row_n );

  protected:
  // -- Protected methods

  // Utility

  void
  remove_empty_groups_at_back ();

  unsigned int
  minor_spacing_max_width ( unsigned int major_sp_width_n ) const;

  private:
  // -- Attributes;
  std::vector< std::unique_ptr< Index > > _indices;
  std::vector< std::unique_ptr< Group > > _groups;
  std::vector< Group * > _groups_active;

  unsigned int _smallest_group_dist;
  unsigned int _smallest_column_dist;

  unsigned int _col_type_count[ 4 ];
  unsigned int _col_type_weights[ 4 ];
  unsigned int _col_type_min_widths[ 4 ];
  unsigned int _col_max_widths[ 4 ];
  unsigned int _col_widths[ 4 ];

  std::vector< unsigned int > _row_min_heights;
  std::vector< unsigned int > _row_heights;
  std::vector< unsigned int > _row_stretch;
  unsigned int _rows_active;

  int _spacing_vertical;

  bool _cache_dirty;
};

} // namespace Wdg::Sliders_Pad::Equal_Columns
