/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <array>
#include <cstddef>

namespace Wdg::Sliders_Pad::Equal_Columns
{

/// @brief Weights
///
class Weights
{
  public:
  // -- Construction

  Weights () { _weights.fill ( 0 ); }

  Weights ( unsigned int * values_n )
  {
    for ( std::size_t ii = 0; ii < _weights.size (); ++ii ) {
      _weights[ ii ] = values_n[ ii ];
    }
  }

  Weights ( unsigned long * values_n )
  {
    for ( std::size_t ii = 0; ii < _weights.size (); ++ii ) {
      _weights[ ii ] = values_n[ ii ];
    }
  }

  Weights ( unsigned long value_all_n ) { _weights.fill ( value_all_n ); }

  ~Weights () = default;

  // -- Setup

  void
  set_all ( unsigned long value_n )
  {
    _weights.fill ( value_n );
  }

  // -- Evaluate

  unsigned long
  sum () const
  {
    unsigned long res = 0;
    for ( const auto & elem : _weights ) {
      res += elem;
    }
    return res;
  }

  void
  normalize ( unsigned long weight_normal_n = 4096 );

  // -- Subscript operators

  unsigned long
  operator[] ( std::size_t idx_n ) const
  {
    return _weights[ idx_n ];
  }

  unsigned long &
  operator[] ( std::size_t idx_n )
  {
    return _weights[ idx_n ];
  }

  private:
  // -- Attributes
  std::array< unsigned long, 4 > _weights;
};

unsigned int
calc_weights_delta_order ( unsigned int * order_n,
                           const Weights & wnorm_req_n,
                           const Weights & wnorm_cur_n );

} // namespace Wdg::Sliders_Pad::Equal_Columns
