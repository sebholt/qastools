/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QObject>
#include <QRect>
#include <QString>
#include <memory>
#include <vector>

// Forward declaration
class QWidget;
namespace Wdg::Sliders_Pad
{
class Data_Group;
class Header_Data;
class Header;
class Footer;
} // namespace Wdg::Sliders_Pad

namespace Wdg::Sliders_Pad
{

/// @brief Data
///
class Data : public QObject
{
  Q_OBJECT

  public:
  // -- Construction

  Data ( QObject * parent_n = nullptr );

  ~Data ();

  // -- Header

  Header *
  header_cast ();

  Footer *
  footer_cast ();

  // -- Groups

  std::size_t
  num_groups () const
  {
    return _groups.size ();
  }

  Data_Group *
  group ( std::size_t index_n ) const
  {
    return _groups[ index_n ].get ();
  }

  void
  reserve_groups ( std::size_t size_n );

  void
  append_group ( Data_Group * group_n );

  void
  clear_groups ();

  // -- Public attributes
  // Areas
  QRect header_area;
  QRect inputs_area; // Contains sliders_area and switches_area
  int sliders_area_y;
  int sliders_area_height;
  int sub_slider_area_y;
  int sub_slider_area_height;
  int switches_area_y;
  int switches_area_height;
  QRect footer_area;
  Header_Data * header_data;
  Header_Data * footer_data;
  QWidget * header = nullptr;
  QWidget * footer = nullptr;
  bool show_value_labels = true;

  private:
  // -- Private attributes
  std::vector< std::unique_ptr< Data_Group > > _groups;
};

} // namespace Wdg::Sliders_Pad
