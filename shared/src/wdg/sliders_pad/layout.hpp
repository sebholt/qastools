/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QLayout>
#include <QObject>
#include <QPen>
#include <QWidget>
#include <memory>

// Forward declarations
namespace Wdg::Sliders_Pad
{
class Data;
class Data_Column;
class Data_Group;
class Header_Data;
class Header_Label;
} // namespace Wdg::Sliders_Pad
namespace Wdg::Sliders_Pad::Equal_Columns
{
class Layout;
}

namespace Wdg::Sliders_Pad
{

/// @brief Layout
///
class Layout : public QLayout
{
  public:
  // -- Construction

  Layout ( Data * sp_data_n, QWidget * parent_n = nullptr );

  ~Layout ();

  // -- Size hints

  QSize
  minimumSize () const override;

  QSize
  sizeHint () const override;

  // -- Header specific

  Header_Data *
  header_data () const
  {
    return _header_data;
  }

  Header_Data *
  footer_data () const
  {
    return _footer_data;
  }

  // -- Header labels

  unsigned int
  num_header_labels () const;

  const Header_Label &
  header_label ( unsigned int idx_n ) const;

  Header_Label &
  header_label ( unsigned int idx_n );

  //
  // QLayout methods
  //

  void
  set_header_item ( QLayoutItem * item_n );

  void
  set_header_widget ( QWidget * wdg_n );

  void
  set_footer_item ( QLayoutItem * item_n );

  void
  set_footer_widget ( QWidget * wdg_n );

  int
  add_group_widget ( QWidget * wdg_n,
                     unsigned int group_idx,
                     unsigned int column_idx,
                     unsigned int row_idx_n );

  void
  addItem ( QLayoutItem * item_n ) override;

  QLayoutItem *
  itemAt ( int index_n ) const override;

  QLayoutItem *
  takeAt ( int index_n ) override;

  int
  count () const override;

  void
  setGeometry ( const QRect & rect_n ) override;

  protected:
  // -- Protected methods

  bool
  extra_sub_slider_spacing () const;

  unsigned int
  header_height_hint ( const QLayoutItem * item_n,
                       const Header_Data * hdata_n ) const;

  void
  calc_column_widths_sync ( unsigned int width_n );

  void
  calc_label_angle ( Header_Data * hdata_n,
                     unsigned int lbl_hor_dist_n,
                     bool min_angle_n );

  double
  calc_label_x_center ( const Header_Data * hdata_n,
                        const Data_Group * sp_grp_n,
                        const Data_Column * sp_col_n );

  unsigned int
  calc_labels_max_x ( const Header_Data * hdata_n );

  void
  calc_columns_sizes ( unsigned int area_width_n, unsigned int area_height_n );

  void
  post_adjust_row_heights ();

  void
  set_geometries ( const QRect & crect_n );

  void
  update_labels_transforms ( Header_Data * hdata_n, const QRect & hrect_n );

  // Private attributes;
  private:
  std::unique_ptr< QLayoutItem > _header_item;
  std::unique_ptr< Wdg::Sliders_Pad::Equal_Columns::Layout > _lay_eqc;
  std::unique_ptr< QLayoutItem > _footer_item;

  std::size_t _num_items = 0;
  QLayoutItem * _items[ 3 ];

  Header_Data * _header_data = nullptr;
  Header_Data * _footer_data = nullptr;

  Data * _sp_data = nullptr;
};

} // namespace Wdg::Sliders_Pad
