/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "wdg/pad_proxy/group.hpp"
#include "wdg/switches_pad/widgets_set.hpp"
#include <QGridLayout>
#include <QPen>
#include <QVBoxLayout>
#include <QWidget>
#include <memory>
#include <vector>

// Forward declaration
namespace Wdg
{
class Style_Db;
}

namespace Wdg::Switches_Pad
{

/// @brief Widgets_Group
///
class Widgets_Group : public QWidget
{
  public:
  // -- Construction

  Widgets_Group ( const Wdg::Style_Db * style_db_n,
                  QWidget * parent_n = nullptr );

  ~Widgets_Group ();

  // -- Style database

  const Wdg::Style_Db *
  style_db () const
  {
    return _style_db;
  }

  // -- Style id

  unsigned int
  style_id () const
  {
    return _style_id;
  }

  void
  set_style_id ( unsigned int id_n );

  // -- Label

  QWidget *
  label ()
  {
    return _label.get ();
  }

  void
  set_label ( QWidget * wdg_n );

  // -- Widgets list

  std::size_t
  num_widgets_sets () const
  {
    return _widgets_sets.size ();
  }

  void
  append_widgets_set ( Widgets_Set * wdg_set_n );

  const Widgets_Set *
  widgets_set ( std::size_t idx_n ) const
  {
    return _widgets_sets[ idx_n ].get ();
  }

  Widgets_Set *
  widgets_set ( std::size_t idx_n )
  {
    return _widgets_sets[ idx_n ].get ();
  }

  protected:
  // -- Event processing

  bool
  eventFilter ( QObject * watched_n, QEvent * event_n ) override;

  void
  paintEvent ( QPaintEvent * event_n ) override;

  private:
  // -- Utilty

  QVBoxLayout *
  lay_vbox ()
  {
    return static_cast< QVBoxLayout * > ( layout () );
  }

  int
  horizontal_spacing () const;

  Q_SLOT
  void
  style_palette_changed ( unsigned int style_id_n );

  private:
  // -- Attributes
  const Wdg::Style_Db * _style_db = nullptr;
  unsigned int _style_id = 0;
  std::unique_ptr< QWidget > _label;
  std::unique_ptr< QGridLayout > _lay_grid;
  std::vector< std::unique_ptr< Widgets_Set > > _widgets_sets;
};

} // namespace Wdg::Switches_Pad
