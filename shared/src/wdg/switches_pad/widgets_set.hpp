/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QWidget>
#include <memory>

namespace Wdg::Switches_Pad
{

/// @brief Widgets_Set
///
class Widgets_Set
{
  public:
  // -- Construction

  Widgets_Set ();

  ~Widgets_Set ();

  // -- Label widget

  QWidget *
  label ()
  {
    return _label.get ();
  }

  void
  set_label ( QWidget * wdg_n );

  // -- Input widget

  void
  set_input ( QWidget * wdg_n );

  QWidget *
  input ()
  {
    return _input.get ();
  }

  private:
  // -- Attributes
  std::unique_ptr< QWidget > _label;
  std::unique_ptr< QWidget > _input;
};

} // namespace Wdg::Switches_Pad
