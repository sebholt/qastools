/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "wdg/pad_focus_info.hpp"
#include <QWidget>
#include <memory>
#include <vector>

// Forward declaration
namespace Wdg::Pad_Proxy
{
class Group;
}
namespace dpe
{
class Image_Allocator;
} // namespace dpe
namespace Wdg
{
class Style_Db;
}

namespace Wdg::Switches_Pad
{

// Forward declaration
class Widgets_Group;

/// @brief Switches pad
///
class Pad : public QWidget
{
  Q_OBJECT

  public:
  // -- Construction

  Pad ( const Wdg::Style_Db * wdg_style_db_n,
        dpe::Image_Allocator * image_alloc_n,
        QWidget * parent_n = nullptr );

  ~Pad ();

  // -- Viewport

  void
  set_viewport_geometry ( const QRect & rect_n );

  // -- Proxies groups

  std::size_t
  num_proxies_groups () const
  {
    return _proxies_groups.size ();
  }

  const std::vector< Wdg::Pad_Proxy::Group * > &
  proxies_groups () const
  {
    return _proxies_groups;
  }

  Wdg::Pad_Proxy::Group *
  proxies_group ( std::size_t idx_n )
  {
    return _proxies_groups[ idx_n ];
  }

  void
  set_proxies_groups (
      const std::vector< Wdg::Pad_Proxy::Group * > & groups_n );

  void
  clear_proxies_groups ();

  // -- Focus item getting / setting

  const Wdg::Pad_Focus_Info &
  focus_info ()
  {
    return _focus_info;
  }

  // -- Public slots

  Q_SLOT
  void
  set_focus_proxy ( unsigned int proxies_group_idx_n );

  Q_SLOT
  void
  set_focus_proxy ( unsigned int proxies_group_idx_n,
                    unsigned int proxy_idx_n );

  // -- Public signals

  Q_SIGNAL
  void
  sig_focus_changed ();

  protected:
  // -- Protected methods

  bool
  event ( QEvent * event_n ) override;

  void
  clear_widgets_groups ();

  void
  create_widgets_groups ();

  private:
  // -- Attributes
  std::vector< Wdg::Pad_Proxy::Group * > _proxies_groups;
  std::vector< std::unique_ptr< Widgets_Group > > _widgets_groups;

  QRect _viewport;
  Pad_Focus_Info _focus_info;

  const Wdg::Style_Db * _wdg_style_db = nullptr;
  dpe::Image_Allocator * _image_alloc = nullptr;
};

} // namespace Wdg::Switches_Pad
