/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QLayoutItem>
#include <QRect>
#include <QSize>
#include <memory>

namespace Wdg::Switches_Pad::Fill_Columns
{

/// @brief Item
///
class Item
{
  public:
  // -- Construction

  Item ();

  Item ( const Item & item_n ) = delete;

  Item ( Item && item_n );

  ~Item ();

  // -- Assignment operators
  Item &
  operator= ( const Item & item_n ) = delete;

  Item &
  operator= ( Item && item_n );

  // -- Attributes
  std::unique_ptr< QLayoutItem > item;
  QSize min_size;
  QRect rect;
};

} // namespace Wdg::Switches_Pad::Fill_Columns
