/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QFrame>
#include <QScrollArea>

namespace Wdg::Switches_Pad
{

/// @brief Scroll area for a Switches_Pad
///
class Scroll_Area : public QScrollArea
{
  public:
  // -- Construction

  Scroll_Area ( QWidget * parent_n = nullptr );

  ~Scroll_Area ();

  // -- Size hints

  QSize
  minimumSizeHint () const override;

  QSize
  sizeHint () const override;

  // -- Widget

  void
  set_widget ( QWidget * wdg_n );

  QWidget *
  take_widget ();

  protected:
  // -- Protected methods

  void
  resizeEvent ( QResizeEvent * event_n ) override;

  bool
  eventFilter ( QObject * watched_n, QEvent * event_n ) override;
};

} // namespace Wdg::Switches_Pad
