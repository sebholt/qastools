/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QObject>
#include <QPalette>
#include <QWidget>
#include <memory>

namespace Wdg::Pad_Proxy
{

// Forward declaration
class Column;
class Group;
class Style;

/// @brief Proxy
///
class Proxy : public QObject
{
  Q_OBJECT;

  public:
  // -- Construction

  Proxy ( QObject * parent_n = nullptr );

  virtual ~Proxy ();

  // -- Proxies

  Column *
  column () const;

  Group *
  group () const;

  // -- Position index

  /// @brief The position index in the parent column
  ///
  unsigned char
  index () const
  {
    return _index;
  }

  void
  set_index ( unsigned char idx_n );

  // -- Focus flag

  bool
  has_focus () const
  {
    return _has_focus;
  }

  void
  set_has_focus ( bool flag_n );

  // -- Enabled flag

  bool
  is_enabled () const
  {
    return _is_enabled;
  }

  void
  set_enabled ( bool flag_n );

  Q_SIGNAL
  void
  sig_enabled_changed ( bool flag_n );

  // -- Widget

  QWidget *
  widget () const
  {
    return _widget;
  }

  void
  set_widget ( QWidget * wdg_n );

  // -- Item name

  const QString &
  item_name () const
  {
    return _item_name;
  }

  void
  set_item_name ( const QString & name_n );

  // -- Group name

  const QString &
  group_name () const
  {
    return _group_name;
  }

  void
  set_group_name ( const QString & name_n );

  // -- Tool tip

  const QString &
  tool_tip () const
  {
    return _tool_tip;
  }

  void
  set_tool_tip ( const QString & tip_n );

  // -- Style variant id

  unsigned int
  variant_id () const
  {
    return _variant_id;
  }

  void
  set_variant_id ( unsigned int id_n );

  // -- Style id

  unsigned int
  style_id () const
  {
    return _style_id;
  }

  void
  set_style_id ( unsigned int id_n );

  // -- Style

  const Style *
  style () const
  {
    return _style.get ();
  }

  void
  set_style ( Style * style_n );

  // -- Event processing

  bool
  eventFilter ( QObject * obj_n, QEvent * event_n ) override;

  // -- Public slots

  Q_SLOT
  virtual void
  update_value_from_source ();

  private:
  // -- Attributes
  unsigned char _index = 0;
  bool _has_focus = false;
  bool _is_enabled = true;

  QString _item_name;
  QString _group_name;
  QString _tool_tip;
  unsigned int _variant_id = 0;
  unsigned int _style_id = 0;

  QWidget * _widget = nullptr;
  std::unique_ptr< Style > _style;
};

} // namespace Wdg::Pad_Proxy
