/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "style.hpp"

namespace Wdg::Pad_Proxy
{

Style::Style () = default;

Style::~Style () = default;

} // namespace Wdg::Pad_Proxy
