/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "wdg/pad_proxy/proxy.hpp"

namespace Wdg::Pad_Proxy
{

/// @brief Enum
///
class Enum : public Proxy
{
  Q_OBJECT

  public:
  // -- Construction

  Enum ( QObject * parent_n = nullptr );

  ~Enum ();

  // -- Enum num items

  int
  enum_num_items () const
  {
    return _enum_num_items;
  }

  void
  set_enum_num_items ( int num_n );

  // -- Enum item name

  virtual QString
  enum_item_name ( int idx_n );

  // -- Enum index

  int
  enum_index () const
  {
    return _enum_index;
  }

  Q_SLOT
  void
  set_enum_index ( int idx_n );

  Q_SIGNAL
  void
  sig_enum_index_changed ( int value_n );

  protected:
  // -- Protected methods

  virtual void
  enum_index_changed ();

  private:
  // -- Attributes
  int _enum_num_items = 0;
  int _enum_index = 0;
};

} // namespace Wdg::Pad_Proxy
