/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "enum.hpp"

namespace Wdg::Pad_Proxy
{

Enum::Enum ( QObject * parent_n )
: Proxy ( parent_n )
{
}

Enum::~Enum () = default;

void
Enum::set_enum_num_items ( int num_n )
{
  _enum_num_items = num_n;
}

QString
Enum::enum_item_name ( int )
{
  // Dummy implementation
  return QString ();
}

void
Enum::set_enum_index ( int idx_n )
{
  if ( idx_n != enum_index () ) {
    _enum_index = idx_n;
    this->enum_index_changed ();
    Q_EMIT sig_enum_index_changed ( enum_index () );
  }
}

void
Enum::enum_index_changed ()
{
  // Dummy implementation
}

} // namespace Wdg::Pad_Proxy
