/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "wdg/pad_proxy/proxy.hpp"

namespace Wdg::Pad_Proxy
{

/// @brief Slider
///
class Slider : public Proxy
{
  Q_OBJECT;

  public:
  // -- Construction

  Slider ( QObject * parent_n = nullptr );

  ~Slider ();

  // -- Slider index

  unsigned long
  slider_index () const
  {
    return _slider_index;
  }

  Q_SLOT
  void
  set_slider_index ( unsigned long idx_n );

  Q_SIGNAL
  void
  sig_slider_index_changed ( unsigned long idx_n );

  // -- Slider index max

  unsigned long
  slider_index_max () const
  {
    return _slider_index_max;
  }

  Q_SLOT
  void
  set_slider_index_max ( unsigned long idx_n );

  Q_SIGNAL
  void
  sig_slider_index_max_changed ( unsigned long idx_n );

  protected:
  // -- Protected methods

  virtual void
  slider_index_changed ();

  virtual void
  slider_index_max_changed ();

  private:
  // -- Attributes
  unsigned long _slider_index = 0;
  unsigned long _slider_index_max = 0;
};

} // namespace Wdg::Pad_Proxy
