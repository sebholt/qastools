/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "dpe/painter.hpp"
#include <QImage>
#include <QString>

namespace Wdg::DS::Painter
{

/// @brief Switch_SVG
///
class Switch_SVG : public dpe::Painter
{
  public:
  // -- Construction

  Switch_SVG ();

  ~Switch_SVG ();

  // -- Base dir

  const QString &
  base_dir () const
  {
    return _base_dir;
  }

  void
  set_base_dir ( const QString & dir_n );

  // -- File prefix

  const QString &
  file_prefix_bg () const
  {
    return _prefix_bg;
  }

  const QString &
  file_prefix_handle () const
  {
    return _prefix_handle;
  }

  void
  set_file_prefix_bg ( const QString & file_prefix_n );

  void
  set_file_prefix_handle ( const QString & file_prefix_n );

  /// @brief Checks if all required SVG images exist
  bool
  ready () const;

  protected:
  // -- Protected methods

  int
  paint_image ( dpe::Paint_Job * pjob_n );

  private:
  // -- Private methods

  int
  paint_bg ( dpe::Paint_Job * pjob_n ) const;

  int
  paint_handle ( dpe::Paint_Job * pjob_n ) const;

  QString
  file_name ( const QString & prefix_n, const QString & suffix_n ) const;

  bool
  file_ready ( const QString & file_name_n ) const;

  /// @return 0 on success (no error)
  int
  render_svg ( dpe::Image & img_n, const QString & svg_file_n ) const;

  private:
  // -- Attributes
  static const unsigned int num_bg = 4;
  static const unsigned int num_handle = 10;

  QString _suffix_bg[ num_bg ];
  QString _suffix_handle[ num_handle ];

  QString _base_dir;
  QString _prefix_bg;
  QString _prefix_handle;
};

} // namespace Wdg::DS::Painter
