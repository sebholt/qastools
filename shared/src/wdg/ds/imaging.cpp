/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "imaging.hpp"
#include "dpe/image_allocator.hpp"
#include "dpe/image_request.hpp"
#include "dpe/image_set_meta.hpp"
#include "wdg/ds/widget_types.hpp"
#include <QWidget>

namespace Wdg::DS
{

Imaging::Imaging ( std::size_t num_image_groups_n,
                   dpe::Image_Allocator * alloc_n )
: _images ( num_image_groups_n )
, _image_alloc ( alloc_n )
, _image_request ( std::make_unique< dpe::Image_Request > ( &_images ) )
{
}

Imaging::~Imaging ()
{
  set_image_alloc ( nullptr );
}

void
Imaging::set_image_alloc ( dpe::Image_Allocator * alloc_n )
{
  if ( _image_alloc == alloc_n ) {
    return;
  }

  if ( _image_alloc != nullptr ) {
    wait_for_request ();
    _image_alloc->return_group ( &_images );
  }

  _image_alloc = alloc_n;
}

void
Imaging::set_images_meta_variant_id ( unsigned int variant_n )
{
  for ( auto & meta : image_request ()->meta ) {
    meta->group_variant = variant_n;
  }
}

void
Imaging::set_images_meta_style_id ( unsigned int style_n )
{
  for ( auto & meta : image_request ()->meta ) {
    meta->style_id = style_n;
  }
}

void
Imaging::set_images_meta_style_sub_id ( unsigned int style_n )
{
  for ( auto & meta : image_request ()->meta ) {
    meta->style_sub_id = style_n;
  }
}

unsigned int
Imaging::style_sub_id ( QWidget * wdg_n )
{
  unsigned int res = Wdg::DS::ST_NORMAL;
  if ( !wdg_n->isEnabled () ) {
    res = Wdg::DS::ST_DISABLED;
  } else if ( !wdg_n->isActiveWindow () ) {
    res = Wdg::DS::ST_INACTIVE;
  }
  return res;
}

void
Imaging::set_images_meta_palette ( const QPalette & pal_n )
{
  for ( auto & meta : image_request ()->meta ) {
    meta->palette = pal_n;
  }
}

} // namespace Wdg::DS
