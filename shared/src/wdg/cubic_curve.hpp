/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <array>

namespace Wdg
{

/// @brief Cubic_Curve
///
class Cubic_Curve
{
  public:
  // -- Construction

  Cubic_Curve ();

  ~Cubic_Curve () = default;

  // -- Evaluation

  double
  eval ( double pos_n ) const;

  double
  eval_speed ( double pos_n ) const;

  void
  set_params ( double x0_n, double v0_n, double x1_n, double time_n );

  private:
  // -- Attributes
  std::array< double, 4 > _coeff;
};

} // namespace Wdg
