/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QLabel>
#include <QString>

namespace Wdg
{

/// @brief Label with a hint text to compute size hints from
///
class Label_Width : public QLabel
{
  public:
  // -- Construction

  Label_Width ( QWidget * parent_n = nullptr );

  ~Label_Width ();

  // -- Minimum width hint tex

  const QString &
  min_text () const
  {
    return _min_text;
  }

  void
  set_min_text ( const QString txt_n );

  // -- Size hints

  QSize
  minimumSizeHint () const override;

  QSize
  sizeHint () const override;

  private:
  // -- Attributes
  QString _min_text;
};

} // namespace Wdg
