/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QColor>

namespace Wdg
{

QColor
col_mix ( const QColor & col_1, const QColor & col_2, int w_1, int w_2 );

} // namespace Wdg
