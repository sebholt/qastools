/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QEvent>

namespace Wdg
{

extern QEvent::Type evt_pass_event_focus;
extern QEvent::Type evt_pass_event_key;

void
init_event_types ();

} // namespace Wdg
