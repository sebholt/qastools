/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <memory>
#include <vector>

namespace QSnd::Mixer
{

// Forward declaration
class Elem;

/// @brief Filter
///
/// Filters mixer elements from a list
///
class Filter
{
  public:
  // -- Public types

  using Element_Vector = std::vector< std::unique_ptr< Elem > >;

  public:
  // -- Construction

  Filter ();

  virtual ~Filter ();

  // -- Filtering

  /// @brief Filters certain elements from accent_n to drop_n
  ///
  virtual void
  filter ( Element_Vector & accept_n, Element_Vector & drop_n ) = 0;
};

} // namespace QSnd::Mixer
