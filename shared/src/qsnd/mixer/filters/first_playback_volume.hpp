/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "qsnd/mixer/filter.hpp"
#include <QString>
#include <vector>

namespace QSnd::Mixer::Filters
{

/// @brief Name filter
///
/// Filters all but the first element with a playback volume
///
class First_Playback_Volume : public Filter
{
  public:
  // -- Construction

  First_Playback_Volume ();

  ~First_Playback_Volume () override;

  // -- Filter

  void
  filter ( Element_Vector & accept_n, Element_Vector & drop_n ) override;
};

} // namespace QSnd::Mixer::Filters
