/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "first_playback_volume.hpp"
#include "qsnd/mixer/elem.hpp"
#include <iostream>

namespace QSnd::Mixer::Filters
{

First_Playback_Volume::First_Playback_Volume () = default;

First_Playback_Volume::~First_Playback_Volume () = default;

void
First_Playback_Volume::filter ( Element_Vector & accept_n,
                                Element_Vector & drop_n )
{
  bool first_found = false;

  auto ait = accept_n.begin ();
  while ( ait != accept_n.end () ) {
    bool keep = false;
    if ( !first_found ) {
      if ( const auto & elem = *ait; elem ) {
        if ( elem->has_volume ( QSnd::Mixer::SD_PLAYBACK ) ) {
          first_found = true;
          keep = true;
        }
      }
    }

    if ( keep ) {
      ++ait;
    } else {
      // Drop element
      drop_n.push_back ( std::move ( *ait ) );
      ait = accept_n.erase ( ait );
    }
  }
}

} // namespace QSnd::Mixer::Filters
