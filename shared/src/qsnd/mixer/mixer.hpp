/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "qsnd/alsa.hpp"
#include <QObject>
#include <memory>
#include <vector>

// Forward declaration
namespace QSnd::Mixer
{
class Elem;
class Filter;
} // namespace QSnd::Mixer
class QSocketNotifier;

namespace QSnd::Mixer
{

/// @brief Mixer_Simple
///
/// Connects ALSA simple mixer objects with Qt objects
///
class Mixer : public QObject
{
  Q_OBJECT

  public:
  // -- Public types

  using Element_Vector = std::vector< std::unique_ptr< QSnd::Mixer::Elem > >;

  public:
  // -- Construction

  Mixer ( QObject * parent_n = nullptr );

  ~Mixer ();

  // -- ALSA structs

  snd_hctl_t *
  snd_hctl () const
  {
    return _snd_hctl;
  }

  snd_mixer_t *
  snd_mixer () const
  {
    return _snd_mixer;
  }

  // -- Elements

  std::size_t
  num_elems () const
  {
    return _mixer_elems.size ();
  }

  const Elem *
  elem ( std::size_t idx_n ) const
  {
    return _mixer_elems[ idx_n ].get ();
  }

  Elem *
  elem ( std::size_t idx_n )
  {
    return _mixer_elems[ idx_n ].get ();
  }

  // -- Element filters

  /// @brief Appends a new element filter
  ///
  /// The ownership of the filter is passed to the mixer.
  void
  append_elem_filter ( std::unique_ptr< Filter > filter_n );

  // -- Open / close

  /// @brief Opens a simple mixer
  ///
  /// @return true on success, false on error
  bool
  open ( const QString & device_address_n );

  /// @brief Closes the mixer and cleans up
  void
  close ();

  bool
  is_open () const
  {
    return ( _snd_mixer != nullptr );
  }

  /// @brief The address that was passed to open()
  const QString &
  device_address () const
  {
    return _device_address;
  }

  // -- Error strings / codes

  const QString &
  err_func () const
  {
    return _err_func;
  }

  const QString &
  err_message () const
  {
    return _err_message;
  }

  // -- Mixer reload

  Q_SIGNAL
  void
  sig_mixer_reload_request ();

  protected:
  // -- Event processing

  bool
  event ( QEvent * event_n ) override;

  private:
  // -- Utility
  friend class QSnd::Mixer::Elem;

  /// @brief Closes the mixer but does not clean up
  void
  close_mixer ();

  Q_SLOT
  void
  socket_event ( int socket_id );

  void
  create_mixer_elems ();

  bool
  create_socket_notifiers ();

  void
  set_socked_notifiers_enabled ( bool flag_n );

  void
  request_reload_later ();

  void
  signalize_all_changes_later ();

  void
  signalize_all_changes ();

  // -- Alsa callbacks

  static int
  alsa_callback_mixer ( snd_mixer_t * snd_mixer_n,
                        unsigned int mask_n,
                        snd_mixer_elem_t * );

  private:
  // -- Attributes
  QString _device_address;
  snd_hctl_t * _snd_hctl = nullptr;
  snd_mixer_t * _snd_mixer = nullptr;

  Element_Vector _mixer_elems;
  std::vector< pollfd > _pollfds;
  std::vector< std::unique_ptr< QSocketNotifier > > _socket_notifiers;

  bool _signalize_changes_requested = false;
  bool _reload_requested = false;

  QString _err_func;
  QString _err_message;

  std::vector< std::unique_ptr< Filter > > _elem_filters;
};

} // namespace QSnd::Mixer
