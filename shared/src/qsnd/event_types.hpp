/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QEvent>

namespace QSnd
{

extern QEvent::Type evt_update_values;
extern QEvent::Type evt_signalize_changes;
extern QEvent::Type evt_reload;

void
init_event_types ();

} // namespace QSnd
