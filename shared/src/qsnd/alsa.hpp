/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <alsa/asoundlib.h>
#include <QString>
#include <string>

namespace QSnd
{

void
print_alsa_error ( const std::string & prefix, int err );

QString
snd_error_qstring ( int err );

} // namespace QSnd
