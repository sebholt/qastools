/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "qsnd/ctl_address_argument.hpp"
#include <QString>

namespace QSnd
{

/// @brief Ctl_Address
///
class Ctl_Address
{
  public:
  // -- Construction

  Ctl_Address ( const char * ctl_str_n = nullptr );

  Ctl_Address ( const QString & ctl_str_n );

  Ctl_Address ( const Ctl_Address & ctl_addr_n );

  Ctl_Address ( Ctl_Address && ctl_addr_n );

  ~Ctl_Address ();

  // -- Clear

  void
  clear ();

  bool
  is_valid () const
  {
    return !_ctl_name.isEmpty ();
  }

  // -- Address string

  const QString &
  addr_str () const
  {
    return _addr_str;
  }

  /// @brief Sets addr_str() and parses the ctl_name and arguments
  void
  set_addr_str ( const QString & addr_str_n );

  // -- Control name

  const QString &
  ctl_name () const
  {
    return _ctl_name;
  }

  /// @brief Sets the ctl_name() without changing the arguments
  void
  set_ctl_name ( const QString & ctl_name );

  // -- Control arguments

  std::size_t
  num_args () const
  {
    return _args.size ();
  }

  const QSnd::Ctl_Address_Argument &
  arg ( std::size_t idx_n ) const
  {
    return _args[ idx_n ];
  }

  void
  append_arg ( const QSnd::Ctl_Address_Argument & arg_n );

  void
  append_arg ( QSnd::Ctl_Address_Argument && arg_n );

  // -- Comparison

  bool
  match ( const QSnd::Ctl_Address & ctl_addr_n ) const;

  // -- Assignment operators

  QSnd::Ctl_Address &
  operator= ( const QSnd::Ctl_Address & ctl_addr_n );

  QSnd::Ctl_Address &
  operator= ( QSnd::Ctl_Address && ctl_addr_n );

  QSnd::Ctl_Address &
  operator= ( const QString & addr_str_n )
  {
    set_addr_str ( addr_str_n );
    return *this;
  }

  private:
  // -- Utility

  void
  update_addr_str ();

  private:
  // -- Attributes
  QString _addr_str;
  QString _ctl_name;
  std::vector< QSnd::Ctl_Address_Argument > _args;
};

} // namespace QSnd
