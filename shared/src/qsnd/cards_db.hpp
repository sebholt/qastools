/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "qsnd/card_info.hpp"
#include <QObject>
#include <memory>
#include <vector>

namespace QSnd
{

/// @brief Cards_Db
///
class Cards_Db : public QObject
{
  Q_OBJECT;

  public:
  // -- Types

  using Card_Handle = std::shared_ptr< const QSnd::Card_Info >;
  using Cards = std::vector< Card_Handle >;

  // -- Properties

  Q_PROPERTY ( int count READ count NOTIFY countChanged )

  // -- Construction

  Cards_Db ( QObject * parent_n = nullptr );

  ~Cards_Db ();

  // -- Cards

  int
  count () const
  {
    return _cards.size ();
  }

  Q_SIGNAL
  void
  countChanged ();

  std::size_t
  num_cards () const
  {
    return _cards.size ();
  }

  const Card_Handle &
  card ( std::size_t index_n ) const
  {
    return _cards[ index_n ];
  }

  const Cards &
  cards () const
  {
    return _cards;
  }

  Q_SIGNAL
  void
  sig_card_new ( std::shared_ptr< const QSnd::Card_Info > handle_n );

  Q_SIGNAL
  void
  sig_card_removed ( std::shared_ptr< const QSnd::Card_Info > handle_n );

  // -- Update

  /// @brief Reload cards list
  Q_SLOT
  void
  reload ();

  private:
  // -- Attributes
  Cards _cards;
};

} // namespace QSnd
