/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <vector>

namespace QSnd::HCtl
{

// Forward declaration
class Elem;

/// @brief Elem_Group
///
class Elem_Group
{
  public:
  // -- Construction

  Elem_Group ();

  ~Elem_Group ();

  // -- Setup

  void
  clear ();

  // -- Elements

  std::size_t
  num_elems () const
  {
    return _snd_elems.size ();
  }

  void
  append_elem ( Elem * elem_n )
  {
    _snd_elems.push_back ( elem_n );
  }

  Elem *
  elem ( std::size_t idx_n ) const
  {
    return _snd_elems[ idx_n ];
  }

  private:
  // -- Attributes
  std::vector< Elem * > _snd_elems;
};

} // namespace QSnd::HCtl
