/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QKeySequence>
#include <QString>

namespace MWdg
{

/// @brief Inputs_Setup
///
class Inputs_Setup
{
  public:
  // -- Construction

  Inputs_Setup ();

  ~Inputs_Setup ();

  // -- Translation

  void
  update_translation ();

  // -- Attributes
  unsigned int wheel_degrees = 720;

  QKeySequence ks_toggle_joined;
  QKeySequence ks_level_channels;
  QKeySequence ks_mute_volumes;

  QKeySequence ks_toggle_vis_stream[ 2 ];

  QString ts_split_channels;
  QString ts_join_channels;
  QString ts_level_channels;
};

} // namespace MWdg
