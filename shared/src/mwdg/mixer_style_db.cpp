/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "mixer_style_db.hpp"
#include "mwdg/mixer_styles.hpp"
#include <iostream>

namespace MWdg
{

Mixer_Style_Db::Mixer_Style_Db ( QObject * parent_n )
: Wdg::Style_Db ( parent_n )
{
  update_palettes ( QPalette () );
}

Mixer_Style_Db::~Mixer_Style_Db () = default;

void
Mixer_Style_Db::update_palettes ( const QPalette & pal_n )
{
  insert_palette ( Wdg::Style_Db::STYLE_DEFAULT, pal_n );
  insert_palette ( MWdg::Mixer_Style::PLAYBACK, palette_playback ( pal_n ) );
  insert_palette ( MWdg::Mixer_Style::CAPTURE, palette_capture ( pal_n ) );
}

void
Mixer_Style_Db::set_force_dark_theme ( bool value_n )
{
  if ( _force_dark_theme != value_n ) {
    _force_dark_theme = value_n;
    update_palettes ( palette_default () );
  }
}

bool
Mixer_Style_Db::detect_dark_theme ( const QPalette & pal_n )
{
  if ( _force_dark_theme ) {
    return true;
  }
  QColor window = pal_n.color ( QPalette::Active, QPalette::Window );
  QColor window_text = pal_n.color ( QPalette::Active, QPalette::WindowText );
  return ( window_text.value () > window.value () );
}

QPalette
Mixer_Style_Db::palette_playback ( const QPalette & pal_default_n )
{
  QPalette pal = pal_default_n;

  const QPalette::ColorGroup grp_act = QPalette::Active;
  const QPalette::ColorGroup grp_iact = QPalette::Inactive;

  if ( !detect_dark_theme ( pal_default_n ) ) {
    // Light theme
    QColor col = QColor ( 15, 15, 242 );
    pal.setColor ( grp_act, QPalette::Window, col );
    pal.setColor ( grp_iact, QPalette::Window, col );

    col = QColor ( 0, 0, 80 );
    pal.setColor ( grp_act, QPalette::WindowText, col );
    pal.setColor ( grp_iact, QPalette::WindowText, col );

    col = QColor ( 255, 255, 180 );
    pal.setColor ( grp_act, QPalette::Light, col );
    pal.setColor ( grp_iact, QPalette::Light, col );
  } else {
    // Dark theme
    QColor col = QColor ( 0, 0, 80 );
    pal.setColor ( grp_act, QPalette::Window, col );
    pal.setColor ( grp_iact, QPalette::Window, col );

    col = QColor ( 180, 180, 255 );
    pal.setColor ( grp_act, QPalette::WindowText, col );
    pal.setColor ( grp_iact, QPalette::WindowText, col );

    // col = QColor ( 255, 255, 180 );
    // pal.setColor ( grp_act, QPalette::Light, col );
    // pal.setColor ( grp_iact, QPalette::Light, col );
  }

  return pal;
}

QPalette
Mixer_Style_Db::palette_capture ( const QPalette & pal_default_n )
{
  QPalette pal = pal_default_n;

  const QPalette::ColorGroup grp_act = QPalette::Active;
  const QPalette::ColorGroup grp_iact = QPalette::Inactive;

  if ( !detect_dark_theme ( pal_default_n ) ) {
    // Light theme
    QColor col = QColor ( 225, 15, 15 );
    pal.setColor ( grp_act, QPalette::Window, col );
    pal.setColor ( grp_iact, QPalette::Window, col );

    col = QColor ( 60, 0, 0 );
    pal.setColor ( grp_act, QPalette::WindowText, col );
    pal.setColor ( grp_iact, QPalette::WindowText, col );

    col = QColor ( 255, 255, 180 );
    pal.setColor ( grp_act, QPalette::Light, col );
    pal.setColor ( grp_iact, QPalette::Light, col );
  } else {
    // Dark theme
    QColor col = QColor ( 80, 0, 0 );
    pal.setColor ( grp_act, QPalette::Window, col );
    pal.setColor ( grp_iact, QPalette::Window, col );

    col = QColor ( 255, 120, 120 );
    pal.setColor ( grp_act, QPalette::WindowText, col );
    pal.setColor ( grp_iact, QPalette::WindowText, col );

    // col = QColor ( 255, 255, 180 );
    // pal.setColor ( grp_act, QPalette::Light, col );
    // pal.setColor ( grp_iact, QPalette::Light, col );
  }

  return pal;
}

} // namespace MWdg
