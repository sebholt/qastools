/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QFontMetrics>
#include <QMargins>
#include <QStyledItemDelegate>

namespace MWdg
{

/// @brief Mixers_Delegate
///
class Mixers_Delegate : public QStyledItemDelegate
{
  Q_OBJECT

  public:
  // -- Construction

  Mixers_Delegate ( QObject * parent_n = nullptr );

  ~Mixers_Delegate ();

  // -- Size hint

  QSize
  sizeHint ( const QStyleOptionViewItem & option_n,
             const QModelIndex & index_n ) const override;

  // -- Paint

  void
  paint ( QPainter * painter_n,
          const QStyleOptionViewItem & option_n,
          const QModelIndex & index_n ) const override;

  private:
  // -- Utility

  QMargins
  item_margins ( const QFontMetrics & fmet_n ) const;

  private:
  // -- Attributes
  unsigned int _vspace = 1;
};

} // namespace MWdg
