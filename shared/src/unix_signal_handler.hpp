/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QObject>
#include <memory>

// Forward declaration
class QSocketNotifier;

class Unix_Signal_Handler : public QObject
{
  Q_OBJECT

  public:
  // -- Construction

  Unix_Signal_Handler ( QObject * parent_n );

  ~Unix_Signal_Handler ();

  static int
  init_unix_signal_handlers ();

  // -- Unix signal handlers.

  static void
  signal_handler_int ( int unused_n );

  static void
  signal_handler_hup ( int unused_n );

  static void
  signal_handler_term ( int unused_n );

  // -- Signals

  Q_SIGNAL
  void
  sig_int ();

  Q_SIGNAL
  void
  sig_hup ();

  Q_SIGNAL
  void
  sig_term ();

  // -- Public slots

  Q_SLOT
  void
  sev_handle_sig_int ();

  Q_SLOT
  void
  sev_handle_sig_hup ();

  Q_SLOT
  void
  sev_handle_sig_term ();

  private:
  // -- Attributes
  static int _sig_int_fds[ 2 ];
  static int _sig_hup_fds[ 2 ];
  static int _sig_term_fds[ 2 ];

  std::unique_ptr< QSocketNotifier > _sn_int;
  std::unique_ptr< QSocketNotifier > _sn_hup;
  std::unique_ptr< QSocketNotifier > _sn_term;
};
