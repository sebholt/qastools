/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "is_buffer_handle.hpp"
#include <QObject>
#include <QTimer>
#include <vector>

namespace dpe
{

/// @brief IS_Buffer
///
class IS_Buffer : public QObject
{
  Q_OBJECT

  public:
  // -- Construction

  IS_Buffer ();

  ~IS_Buffer ();

  void
  clear ();

  // Buffer info

  std::size_t
  num_sets () const
  {
    return _handles.size ();
  }

  std::size_t
  byte_count () const;

  // Image set accquiring an returning

  dpe::IS_Buffer_Handle *
  acquire_return_handle ( const dpe::Image_Set_Meta * meta_n,
                          dpe::Image_Set * cur_set_n = nullptr );

  void
  append_handle ( std::unique_ptr< dpe::IS_Buffer_Handle > handle_n );

  void
  return_img_set ( dpe::Image_Set * img_set_n );

  private:
  // -- Private slots

  Q_SLOT
  void
  remove_poll ();

  private:
  // -- Attributes
  std::vector< std::unique_ptr< dpe::IS_Buffer_Handle > > _handles;
  QTimer _remove_poll_timer;
  std::size_t _storage_limit;
};

} // namespace dpe
