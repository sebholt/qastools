/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "image_set.hpp"

namespace dpe
{

Image_Set::Image_Set ( std::size_t num_images_n )
: _images ( num_images_n )
{
}

Image_Set::~Image_Set () {}

std::size_t
Image_Set::byte_count () const
{
  std::size_t cnt = 0;
  for ( const auto & image : _images ) {
    cnt += image.byte_count ();
  }
  return cnt;
}

void
Image_Set::convert_to_pixmap ()
{
  for ( auto & image : _images ) {
    image.convert_to_pixmap ();
  }
}

} // namespace dpe
