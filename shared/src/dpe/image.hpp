/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QPixmap>
#include <memory>

namespace dpe
{

/// @brief Image
///
/// An image set that can be shared by multiple users.
/// Right after creation the user count is always 0.
///
class Image
{
  public:
  // -- Construction

  Image ();

  ~Image ();

  // -- Setup

  void
  clear ();

  // -- Sizes

  void
  set_size ( unsigned int width_n,
             unsigned int height_n,
             unsigned int stride_n );

  unsigned int
  width () const
  {
    return _width;
  }

  unsigned int
  height () const
  {
    return _height;
  }

  unsigned int
  stride () const
  {
    return _stride;
  }

  unsigned int
  byte_count () const
  {
    return height () * stride ();
  }

  // -- Pixmap

  QPixmap *
  convert_to_pixmap ();

  QPixmap *
  pixmap () const
  {
    return _pixmap.get ();
  }

  // -- Image

  QImage &
  qimage ()
  {
    return _qimage;
  }

  private:
  // -- Attributes
  std::unique_ptr< QPixmap > _pixmap;
  QImage _qimage;

  unsigned int _width = 0;
  unsigned int _height = 0;
  unsigned int _stride = 0;
};

} // namespace dpe
