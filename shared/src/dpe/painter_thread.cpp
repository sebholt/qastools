/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "painter_thread.hpp"
#include "painter_thread_shared.hpp"
#include <iostream>

namespace dpe
{

Painter_Thread::Painter_Thread (
    const std::shared_ptr< dpe::Painter_Thread_Shared > & shared_n )
: _shared ( shared_n )
{
}

Painter_Thread::~Painter_Thread () = default;

void
Painter_Thread::run ()
{
  while ( _shared->process_job () ) {
  }
}

} // namespace dpe
