/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QTimer>
#include <memory>
#include <vector>

// Forward declaration
namespace dpe
{
class Image_Set_Group;
class Image_Request;
class IS_Buffer;
class IS_Buffer_Handle;
class Painter;
class Painter_Thread;
class Painter_Thread_Shared;
} // namespace dpe

namespace dpe
{

/// @brief Image_Set_Group allocator
///
class Image_Allocator : public QObject
{
  Q_OBJECT

  public:
  // -- Construction

  Image_Allocator ( QObject * parent_n = nullptr );

  ~Image_Allocator () override;

  // -- Painters

  void
  install_painter ( const std::shared_ptr< dpe::Painter > & painter_n );

  // IO

  void
  send_request ( dpe::Image_Request * request_n );

  void
  return_group ( dpe::Image_Set_Group * group_n );

  private:
  // -- Private slots

  Q_SLOT
  void
  stop_timeout ();

  private:
  // -- Private methods

  void
  start_threads ();

  void
  stop_threads ();

  void
  enqueue_handle ( dpe::IS_Buffer_Handle * handle_n );

  private:
  // -- Attributes
  std::unique_ptr< dpe::IS_Buffer > _buffer;
  std::shared_ptr< dpe::Painter_Thread_Shared > _shared;
  std::size_t _num_threads = 1;
  std::vector< std::unique_ptr< dpe::Painter_Thread > > _threads;
  QTimer _stop_timer;
};

} // namespace dpe
