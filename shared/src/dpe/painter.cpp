/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "painter.hpp"
#include "dpe/image.hpp"
#include "dpe/image_set_meta.hpp"
#include "image_set_meta.hpp"
#include "paint_job.hpp"

namespace dpe
{

Painter::Painter ( unsigned int group_type_n, unsigned int group_variant_n )
: _group_type ( group_type_n )
, _group_variant ( group_variant_n )
{
}

Painter::~Painter () {}

void
Painter::set_group_variant ( unsigned int variant_n )
{
  _group_variant = variant_n;
}

int
Painter::check_and_paint ( dpe::Paint_Job * pjob_n )
{
  // Check if meta data is available
  if ( pjob_n->meta == nullptr ) {
    return -1;
  }
  // Check image size
  {
    const QSize & isize = pjob_n->meta->size;
    if ( ( isize.width () <= 0 ) || ( isize.height () <= 0 ) ) {
      return -1;
    }
  }
  // Paint
  return this->paint_image ( pjob_n );
}

int
Painter::create_image_data ( dpe::Image * img_n,
                             const dpe::Image_Set_Meta * meta_n )
{
  if ( !meta_n->size.isValid () ) {
    return -1;
  }

  img_n->set_size ( meta_n->size.width (),
                    meta_n->size.height (),
                    meta_n->size.width () * 4 );
  img_n->qimage ().fill ( 0 );

  return 0;
}

} // namespace dpe
