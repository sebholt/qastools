/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QElapsedTimer>
#include <memory>

// Forward declaration
namespace dpe
{
class Image_Set;
class Image_Set_Meta;
class Image_Set_State;
} // namespace dpe

namespace dpe
{

/// @brief IS_Buffer_Handle
///
class IS_Buffer_Handle
{
  public:
  // -- Construction

  IS_Buffer_Handle ();

  ~IS_Buffer_Handle ();

  // Public attributes
  std::unique_ptr< dpe::Image_Set > img_set;
  std::unique_ptr< dpe::Image_Set_Meta > meta;

  std::size_t num_users = 0;
  QElapsedTimer remove_time;

  std::unique_ptr< dpe::Image_Set_State > state;
};

} // namespace dpe
