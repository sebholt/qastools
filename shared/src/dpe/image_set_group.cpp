/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "image_set_group.hpp"
#include "image_set.hpp"

namespace dpe
{

Image_Set_Group::Image_Set_Group ( std::size_t num_sets_n )
: img_sets ( num_sets_n, nullptr )
{
}

Image_Set_Group::~Image_Set_Group () = default;

bool
Image_Set_Group::ready () const
{
  for ( const auto & image_set : img_sets ) {
    if ( image_set == nullptr ) {
      return false;
    }
  }
  return true;
}

void
Image_Set_Group::convert_to_pixmap ()
{
  for ( auto & image_set : img_sets ) {
    image_set->convert_to_pixmap ();
  }
}

} // namespace dpe
