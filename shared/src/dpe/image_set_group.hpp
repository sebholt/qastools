/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <vector>

// Forward declaration
namespace dpe
{
class Image_Set;
}

namespace dpe
{

/// @brief Image_Set_Group
///
class Image_Set_Group
{
  public:
  // -- Construction

  Image_Set_Group ( std::size_t num_sets_n = 0 );

  ~Image_Set_Group ();

  bool
  ready () const;

  void
  convert_to_pixmap ();

  public:
  // -- Attributes
  std::vector< dpe::Image_Set * > img_sets;
};

} // namespace dpe
