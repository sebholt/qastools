/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "image.hpp"
#include <vector>

namespace dpe
{

/// @brief Image_Set
///
/// An image set that can be shared by multiple users.
/// Right after creation the user count is always 0.
///
class Image_Set
{
  public:
  // -- Construction

  Image_Set ( std::size_t num_images_n );

  virtual ~Image_Set ();

  // -- Pixmap

  void
  convert_to_pixmap ();

  QPixmap *
  convert_to_pixmap ( std::size_t idx_n )
  {
    return image ( idx_n ).convert_to_pixmap ();
  }

  // Number of images

  std::size_t
  num_images () const
  {
    return _images.size ();
  }

  std::size_t
  byte_count () const;

  const dpe::Image &
  image ( std::size_t index_n ) const
  {
    return _images[ index_n ];
  }

  dpe::Image &
  image ( std::size_t index_n )
  {
    return _images[ index_n ];
  }

  QPixmap *
  pixmap ( std::size_t index_n )
  {
    return image ( index_n ).pixmap ();
  }

  private:
  // -- Attributes
  std::vector< dpe::Image > _images;
};

} // namespace dpe
