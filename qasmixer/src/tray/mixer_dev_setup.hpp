/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QString>

namespace Tray
{

/// @brief Mixer_Dev_Setup
///
class Mixer_Dev_Setup
{
  public:
  // -- Public types

  enum Mixer_Device
  {
    MIXER_DEV_DEFAULT = 0,
    MIXER_DEV_CURRENT = 1,
    MIXER_DEV_USER = 2,
    MIXER_DEV_LAST = MIXER_DEV_USER
  };

  // -- Construction

  Mixer_Dev_Setup ();

  ~Mixer_Dev_Setup ();

  public:
  // -- Attributes
  /// @brief Which device to use for the mixer
  Mixer_Device device_mode = Mixer_Device::MIXER_DEV_DEFAULT;

  /// @brief Mixer device of the main mixer window
  QString current_device;

  /// @brief User defined mixer device
  QString user_device;
};

} // namespace Tray
