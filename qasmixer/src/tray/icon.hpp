/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "tray/shared.hpp"
#include <QMenu>
#include <QObject>
#include <QSystemTrayIcon>

namespace Tray
{

/// @brief System tray icon
///
class Icon : public QSystemTrayIcon
{
  Q_OBJECT

  public:
  // -- Construction

  Icon ( const Shared & shared_n, QObject * parent_n = nullptr );

  ~Icon () override;

  // -- Public signals

  Q_SIGNAL
  void
  sig_hover ();

  Q_SIGNAL
  void
  sig_activated ();

  Q_SIGNAL
  void
  sig_middle_click ();

  Q_SIGNAL
  void
  sig_wheel_delta ( int delta_n );

  Q_SIGNAL
  void
  sig_quit ();

  // -- Utility

  void
  update_icon ();

  // -- Event processing

  bool
  event ( QEvent * event_n ) override;

  protected:
  // -- Protected slots

  Q_SLOT
  void
  activation ( QSystemTrayIcon::ActivationReason reason_n );

  private:
  // -- Attributes
  const Shared & _shared;
  Shared::Icon_Index _icon_index = Shared::ICI_VOLUME_LOW;
  QMenu _cmenu;
};

} // namespace Tray
