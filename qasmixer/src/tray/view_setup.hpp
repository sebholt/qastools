/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

namespace Tray
{

/// @brief View_Setup
///
class View_Setup
{
  public:
  // -- Construction

  View_Setup ();

  ~View_Setup ();

  public:
  // -- Attributes
  /// @brief Balloon lifetime in ms
  unsigned int balloon_lifetime = 4000;
  /// @brief Mouse wheel degrees for slider widgets
  unsigned int wheel_degrees = 720;
  /// @brief Whether to show the balloon
  bool show_balloon = true;
};

} // namespace Tray
