/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QIcon>
#include <QObject>
#include <QString>
#include <array>

namespace Tray
{

/// @brief Shared
///
class Shared : public QObject
{
  Q_OBJECT;

  public:
  // -- Public types

  enum Icon_Index
  {
    ICI_MUTED = 0,
    ICI_VOLUME_LOW = 1,
    ICI_VOLUME_MEDIUM = 2,
    ICI_VOLUME_HIGH = 3,
    ICI_COUNT = 4
  };

  // -- Construction

  Shared ( QObject * parent_n = nullptr );

  ~Shared ();

  void
  update_volume_icon ();

  // -- Public attributes
  struct
  {
    unsigned int permille = 0;
    bool muted = false;
    Icon_Index icon_index = Icon_Index::ICI_VOLUME_LOW;
    QString icon_name;
  } volume;

  struct
  {
    QString volume_muted;
    QString volume_low;
    QString volume_medium;
    QString volume_high;
  } icon_names;

  std::array< QIcon, ICI_COUNT > volume_icons;

  struct
  {
    QString volume_percent_mask;
    QString volume_change;
    QString muted;
  } l10n_strings;

  QString application_title;
};

} // namespace Tray
