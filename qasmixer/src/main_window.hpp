/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "dpe/image_allocator.hpp"
#include "main_window_setup.hpp"
#include "mwdg/mixer_style_db.hpp"
#include "qsnd/ctl_address.hpp"
#include <QAction>
#include <QActionGroup>
#include <QDialog>
#include <QMainWindow>
#include <QPointer>
#include <QSplitter>
#include <memory>

// Forward declaration
namespace MState::Mixer
{
class States_Db;
}
namespace MWdg
{
class Stream_View_Selection_Actions;
}
namespace Views
{
class Mixer;
class Device_Selection_Bar;
} // namespace Views
namespace QSnd
{
class Cards_Db;
}

/// @brief Main_Window
///
class Main_Window : public QMainWindow
{
  Q_OBJECT

  public:
  // -- Construction

  Main_Window ( QSnd::Cards_Db * cards_db_n,
                MState::Mixer::States_Db * mixer_states_db_n,
                QWidget * parent_n = nullptr,
                Qt::WindowFlags flags_n = Qt::WindowFlags () );

  ~Main_Window ();

  // -- Size hints

  QSize
  sizeHint () const override;

  // -- Setup

  void
  set_window_setup ( Main_Window_Setup * setup_n );

  void
  select_ctl ( const QSnd::Ctl_Address & ctl_n );

  // -- Signals

  Q_SIGNAL
  void
  sig_show_settings ();

  Q_SIGNAL
  void
  sig_show_about ();

  Q_SIGNAL
  void
  sig_show_about_qt ();

  Q_SIGNAL
  void
  sig_control_changed ();

  Q_SIGNAL
  void
  sig_quit ();

  // -- Public slots

  Q_SLOT
  void
  select_ctl_from_side_iface ();

  Q_SLOT
  void
  reload_mixer_device ();

  Q_SLOT
  void
  reload_mixer_inputs ();

  Q_SLOT
  void
  reload_mixer_view ();

  Q_SLOT
  void
  refresh_views ();

  /// @brief Sets/unsets fullscreen mode
  ///
  Q_SLOT
  void
  set_fullscreen ( bool flag_n );

  // Device selection

  Q_SLOT
  void
  show_device_selection ( bool flag_n );

  Q_SLOT
  void
  toggle_device_selection ();

  // Device information dialog

  Q_SLOT
  void
  toggle_device_settings_dialog ();

  // State

  /// @brief Save state to the setup tree
  ///
  Q_SLOT
  void
  save_state ();

  protected:
  // -- Protected methods

  Q_SLOT
  void
  on_splitter_moved ();

  void
  update_fullscreen_action ();

  // Event handlers

  void
  changeEvent ( QEvent * event_n ) override;

  void
  keyPressEvent ( QKeyEvent * event_n ) override;

  void
  closeEvent ( QCloseEvent * event_n ) override;

  private:
  // -- Attributes
  Main_Window_Setup * _win_setup = nullptr;
  MWdg::Mixer_Style_Db _mixer_style_db;
  dpe::Image_Allocator _image_alloc;
  QSnd::Cards_Db * _cards_db = nullptr;

  // Base widgets
  std::unique_ptr< QSplitter > _splitter;
  std::unique_ptr< Views::Mixer > _mixer;
  std::unique_ptr< Views::Device_Selection_Bar > _dev_select;

  // Dialogs
  QPointer< QDialog > _dialog_device_settings;

  // Menubar
  std::unique_ptr< QAction > _act_show_dev_select;
  std::unique_ptr< QAction > _act_fullscreen;
  std::unique_ptr< MWdg::Stream_View_Selection_Actions > _acts_stream_view;

  // Strings and icons
  QString _str_fscreen_enable;
  QString _str_fscreen_disable;
  QIcon _icon_fscreen_enable;
  QIcon _icon_fscreen_disable;
};
