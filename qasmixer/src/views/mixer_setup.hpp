/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "views/view_base_setup.hpp"

namespace Views
{

/// @brief Mixer_Setup
///
class Mixer_Setup : public Views::View_Base_Setup
{
  public:
  // -- Construction

  Mixer_Setup ();

  ~Mixer_Setup () override;

  public:
  // -- Attributes
  bool show_slider_value_labels = true;
  bool show_tool_tips = true;
};

} // namespace Views
