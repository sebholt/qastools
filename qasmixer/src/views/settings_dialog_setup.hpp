/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QByteArray>
#include <cstddef>

namespace Views
{

/// @brief Settings_Dialog_Setup
///
class Settings_Dialog_Setup
{
  public:
  // -- Construction

  Settings_Dialog_Setup ();

  ~Settings_Dialog_Setup ();

  public:
  // -- Attributes
  std::size_t page = 0;
  QByteArray window_geometry;
  QByteArray splitter_state;
};

} // namespace Views
