/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QByteArray>
#include <cstddef>

namespace Views
{

/// @brief Device_Settings_Dialog_Setup
///
class Device_Settings_Dialog_Setup
{
  public:
  // -- Construction

  Device_Settings_Dialog_Setup ();

  ~Device_Settings_Dialog_Setup ();

  public:
  // -- Attributes
  std::size_t page = 0;
  QByteArray window_geometry;
  QByteArray splitter_state;
  QByteArray controls_header_view_state;
};

} // namespace Views
