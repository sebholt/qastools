/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "mwdg/inputs_setup.hpp"
#include "mwdg/mixer_device_setup.hpp"
#include "views/device_selection_view_setup.hpp"
#include "views/device_settings_dialog_setup.hpp"
#include "views/mixer_setup.hpp"
#include <QByteArray>

/// @brief Main_Window_Setup
///
class Main_Window_Setup
{
  public:
  // -- Construction

  Main_Window_Setup ();

  ~Main_Window_Setup ();

  public:
  // -- Attributes
  bool show_dev_select = true;
  QByteArray window_state;
  QByteArray window_geometry;
  QByteArray splitter_state;

  MWdg::Mixer_Device_Setup mixer_dev;
  MWdg::Inputs_Setup inputs;

  Views::Mixer_Setup mixer;
  Views::Device_Selection_View_Setup dev_select;
  Views::Device_Settings_Dialog_Setup device_settings_dialog;
};
