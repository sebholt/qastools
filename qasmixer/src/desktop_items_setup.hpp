/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "main_window_setup.hpp"
#include "tray/mixer_dev_setup.hpp"
#include "tray/view_setup.hpp"
#include "views/settings_dialog_setup.hpp"
#include <QString>

/// @brief Desktop_Items_Setup
///
class Desktop_Items_Setup
{
  public:
  // -- Public types

  enum Mixer_Device
  {
    MIXER_DEV_DEFAULT = 0,
    MIXER_DEV_PREVIOUS = 1,
    MIXER_DEV_USER = 2,
    MIXER_DEV_LAST = MIXER_DEV_USER
  };

  // -- Construction

  Desktop_Items_Setup ();

  ~Desktop_Items_Setup ();

  // -- Storage

  void
  read_from_storage ();

  void
  write_to_storage ();

  public:
  // -- Attributes
  // Device to load on startup
  Mixer_Device start_device_mode = Mixer_Device::MIXER_DEV_DEFAULT;
  QString start_user_device;

  Tray::Mixer_Dev_Setup tray_mixer_dev;
  Tray::View_Setup tray_view;
  Main_Window_Setup main_window;
  Views::Settings_Dialog_Setup settings_dialog;

  bool tray_on_close = true;
  bool tray_show_icon = true;
  bool tray_is_minimized = false;
};
