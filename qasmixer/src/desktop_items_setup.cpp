/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "desktop_items_setup.hpp"
#include <QSettings>
#include <iostream>

Desktop_Items_Setup::Desktop_Items_Setup ()
: start_user_device ( "hw:0" )
{
}

Desktop_Items_Setup::~Desktop_Items_Setup () = default;

void
Desktop_Items_Setup::read_from_storage ()
{
  Main_Window_Setup & mwin = main_window;

  QSettings settings;

  start_device_mode = static_cast< Desktop_Items_Setup::Mixer_Device > (
      settings.value ( "start_device_mode", start_device_mode ).toUInt () );

  start_user_device =
      settings.value ( "start_user_device", start_user_device ).toString ();

  mwin.mixer_dev.control_address =
      settings
          .value ( "current_device",
                   mwin.mixer_dev.control_address.addr_str () )
          .toString ();

  mwin.inputs.wheel_degrees =
      settings.value ( "wheel_degrees", mwin.inputs.wheel_degrees ).toUInt ();

  tray_on_close = settings.value ( "tray_on_close", tray_on_close ).toBool ();

  tray_show_icon =
      settings.value ( "tray_show_icon", tray_show_icon ).toBool ();

  tray_is_minimized =
      settings.value ( "tray_is_minimized", tray_is_minimized ).toBool ();

  // Main window state
  {
    settings.beginGroup ( "main_window" );

    mwin.window_state =
        settings.value ( "window_state", mwin.window_state ).toByteArray ();

    mwin.window_geometry =
        settings.value ( "window_geometry", mwin.window_geometry )
            .toByteArray ();

    mwin.splitter_state =
        settings.value ( "splitter_state", mwin.splitter_state ).toByteArray ();

    mwin.show_dev_select =
        settings.value ( "show_device_selection", mwin.show_dev_select )
            .toBool ();

    settings.endGroup ();
  }

  // Device selection
  {
    Views::Device_Selection_View_Setup & vsetup ( main_window.dev_select );

    settings.beginGroup ( "device_selection" );

    vsetup.user_device =
        settings.value ( "user_device", vsetup.user_device ).toString ();

    settings.endGroup ();
  }

  // Mixer setup
  {
    Views::Mixer_Setup & vsetup ( main_window.mixer );

    settings.beginGroup ( "simple_mixer" );

    vsetup.show_slider_value_labels =
        settings
            .value ( "show_slider_value_labels",
                     vsetup.show_slider_value_labels )
            .toBool ();

    vsetup.show_tool_tips =
        settings.value ( "show_tool_tips", vsetup.show_tool_tips ).toBool ();

    settings.endGroup ();
  }

  // Tray mixer
  {
    settings.beginGroup ( "tray_mixer" );

    Tray::Mixer_Dev_Setup & dsetup ( tray_mixer_dev );

    dsetup.device_mode = static_cast< Tray::Mixer_Dev_Setup::Mixer_Device > (
        settings.value ( "device_mode", dsetup.device_mode ).toInt () );

    dsetup.user_device =
        settings.value ( "user_device", dsetup.user_device ).toString ();

    Tray::View_Setup & vsetup ( tray_view );

    vsetup.show_balloon =
        settings.value ( "show_balloon", vsetup.show_balloon ).toBool ();

    vsetup.balloon_lifetime =
        settings.value ( "balloon_lifetime", vsetup.balloon_lifetime )
            .toUInt ();

    settings.endGroup ();
  }

  // Settings dialog
  {
    settings.beginGroup ( "settings_dialog" );

    {
      auto & sval = settings_dialog.page;
      sval = settings.value ( "page", static_cast< unsigned int > ( sval ) )
                 .toUInt ();
    }

    {
      auto & sval = settings_dialog.window_geometry;
      sval = settings.value ( "window_geometry", sval ).toByteArray ();
    }

    {
      auto & sval = settings_dialog.splitter_state;
      sval = settings.value ( "splitter_state", sval ).toByteArray ();
    }

    settings.endGroup ();
  }

  // Device settings dialog
  {
    settings.beginGroup ( "device_settings_dialog" );

    {
      auto & sval = mwin.device_settings_dialog.page;
      sval = settings.value ( "page", static_cast< unsigned int > ( sval ) )
                 .toUInt ();
    }

    {
      auto & sval = mwin.device_settings_dialog.window_geometry;
      sval = settings.value ( "window_geometry", sval ).toByteArray ();
    }

    {
      auto & sval = mwin.device_settings_dialog.splitter_state;
      sval = settings.value ( "splitter_state", sval ).toByteArray ();
    }

    {
      auto & sval = mwin.device_settings_dialog.controls_header_view_state;
      sval =
          settings.value ( "controls_header_view_state", sval ).toByteArray ();
    }

    settings.endGroup ();
  }

  // Sanitize values

  if ( main_window.inputs.wheel_degrees == 0 ) {
    main_window.inputs.wheel_degrees = 720;
  }
  tray_view.wheel_degrees = main_window.inputs.wheel_degrees;

  if ( start_device_mode > Desktop_Items_Setup::MIXER_DEV_LAST ) {
    start_device_mode = Desktop_Items_Setup::MIXER_DEV_DEFAULT;
  }

  if ( tray_mixer_dev.device_mode > Tray::Mixer_Dev_Setup::MIXER_DEV_LAST ) {
    tray_mixer_dev.device_mode = Tray::Mixer_Dev_Setup::MIXER_DEV_DEFAULT;
  }
}

void
Desktop_Items_Setup::write_to_storage ()
{
  // std::cout << "Desktop_Items_Setup::write_to_storage" << "\n";

  QSettings settings;

  // General

  settings.setValue ( "start_device_mode", start_device_mode );

  settings.setValue ( "start_user_device", start_user_device );

  settings.setValue ( "current_device",
                      main_window.mixer_dev.control_address.addr_str () );

  settings.setValue ( "wheel_degrees", main_window.inputs.wheel_degrees );

  settings.setValue ( "tray_on_close", tray_on_close );

  settings.setValue ( "tray_show_icon", tray_show_icon );

  settings.setValue ( "tray_is_minimized", tray_is_minimized );

  // Main window state
  {
    settings.beginGroup ( "main_window" );

    settings.setValue ( "window_state", main_window.window_state );

    settings.setValue ( "window_geometry", main_window.window_geometry );

    settings.setValue ( "splitter_state", main_window.splitter_state );

    settings.setValue ( "show_device_selection", main_window.show_dev_select );

    settings.endGroup ();
  }

  // Device selection
  {
    const Views::Device_Selection_View_Setup & vsetup (
        main_window.dev_select );
    settings.beginGroup ( "device_selection" );

    settings.setValue ( "user_device", vsetup.user_device );

    settings.endGroup ();
  }

  // Simple mixer
  {
    const Views::Mixer_Setup & vsetup ( main_window.mixer );

    settings.beginGroup ( "simple_mixer" );

    settings.setValue ( "show_slider_value_labels",
                        vsetup.show_slider_value_labels );

    settings.setValue ( "show_tool_tips", vsetup.show_tool_tips );

    settings.endGroup ();
  }

  // Mini mixer
  {

    settings.beginGroup ( "tray_mixer" );

    const Tray::Mixer_Dev_Setup & dsetup ( tray_mixer_dev );

    settings.setValue ( "device_mode", dsetup.device_mode );

    settings.setValue ( "user_device", dsetup.user_device );

    const Tray::View_Setup & vsetup ( tray_view );

    settings.setValue ( "show_balloon", vsetup.show_balloon );

    settings.setValue ( "balloon_lifetime", vsetup.balloon_lifetime );

    settings.endGroup ();
  }

  // Settings dialog
  {
    settings.beginGroup ( "settings_dialog" );

    settings.setValue ( "page",
                        static_cast< unsigned int > ( settings_dialog.page ) );

    settings.setValue ( "window_geometry", settings_dialog.window_geometry );

    settings.setValue ( "splitter_state", settings_dialog.splitter_state );

    settings.endGroup ();
  }

  // Device settings dialog
  {
    settings.beginGroup ( "device_settings_dialog" );

    settings.setValue ( "page",
                        static_cast< unsigned int > (
                            main_window.device_settings_dialog.page ) );

    settings.setValue ( "window_geometry",
                        main_window.device_settings_dialog.window_geometry );

    settings.setValue ( "splitter_state",
                        main_window.device_settings_dialog.splitter_state );

    settings.setValue (
        "controls_header_view_state",
        main_window.device_settings_dialog.controls_header_view_state );

    settings.endGroup ();
  }
}
