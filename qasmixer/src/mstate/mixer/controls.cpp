/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "controls.hpp"
#include <iostream>

namespace MState::Mixer
{

Controls::Controls () = default;

Controls::~Controls () = default;

bool
Controls::proxies_group_separated ( const QString & group_id_n ) const
{
  return _separated_proxies_groups.contains ( group_id_n );
}

void
Controls::set_proxies_group_separated ( const QString & group_id_n,
                                        bool separated_n )
{
  if ( separated_n ) {
    _separated_proxies_groups.insert ( group_id_n );
  } else {
    _separated_proxies_groups.remove ( group_id_n );
  }
}

bool
Controls::proxies_group_blacklisted ( const QString & group_id_n ) const
{
  return _blacklisted_proxies_groups.contains ( group_id_n );
}

void
Controls::set_proxies_group_blacklisted ( const QString & group_id_n,
                                          bool blacklisted_n )
{
  if ( blacklisted_n ) {
    _blacklisted_proxies_groups.insert ( group_id_n );
  } else {
    _blacklisted_proxies_groups.remove ( group_id_n );
  }
}

} // namespace MState::Mixer
