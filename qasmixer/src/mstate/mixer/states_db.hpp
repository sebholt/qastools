/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QByteArray>
#include <QHash>
#include <QString>
#include <memory>

// Forward declaration
namespace QSnd
{
class Cards_Db;
class Ctl_Address;
} // namespace QSnd
namespace MState::Mixer
{
class State;
}

namespace MState::Mixer
{

/// @brief States
///
class States_Db
{
  public:
  // -- Construction

  States_Db ( QSnd::Cards_Db * cards_db_n );

  ~States_Db ();

  // -- Cards database

  QSnd::Cards_Db *
  cards_db () const
  {
    return _cards_db;
  }

  // -- Storage

  void
  read_from_storage ();

  void
  write_to_storage ();

  // -- State

  const std::shared_ptr< Mixer::State > &
  state ( const QSnd::Ctl_Address & addr_n );

  private:
  // -- Utility

  QByteArray
  read_config_file () const;

  private:
  // -- Attributes
  QString _config_file;
  QSnd::Cards_Db * _cards_db = nullptr;
  QHash< QString, std::shared_ptr< Mixer::State > > _states;
  std::shared_ptr< Mixer::State > _empty_state;
};

} // namespace MState::Mixer
