/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "mstate/mixer/controls.hpp"
#include <QList>
#include <array>

namespace MState::Mixer
{

/// @brief State
///
class State
{
  public:
  // -- Construction

  State ();

  ~State ();

  // -- Splitter

  const QList< int > &
  splitter_sizes () const
  {
    return _splitter_sizes;
  }

  void
  set_splitter_sizes ( const QList< int > & list_n );

  // -- Show stream

  std::array< bool, 2 > &
  show_stream ()
  {
    return _show_stream;
  }

  const std::array< bool, 2 > &
  show_stream () const
  {
    return _show_stream;
  }

  // -- Sorting

  /// @brief Sort by stream direction (playback,capture)
  bool
  sort_stream_dir () const
  {
    return _sort_stream_dir;
  }

  void
  set_sort_stream_dir ( bool flag_n )
  {
    _sort_stream_dir = flag_n;
  }

  // -- Sliders

  Controls &
  sliders ()
  {
    return _sliders;
  }

  const Controls &
  sliders () const
  {
    return _sliders;
  }

  // -- Switches

  Controls &
  switches ()
  {
    return _switches;
  }

  const Controls &
  switches () const
  {
    return _switches;
  }

  private:
  // -- Attributes
  QList< int > _splitter_sizes;
  std::array< bool, 2 > _show_stream;
  bool _sort_stream_dir = false;
  Controls _sliders;
  Controls _switches;
};

} // namespace MState::Mixer
