/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QSet>
#include <QString>

namespace MState::Mixer
{

/// @brief Controls
///
class Controls
{
  public:
  // -- Construction

  Controls ();

  ~Controls ();

  // -- Proxies group separation

  const QSet< QString > &
  separated_proxies_groups () const
  {
    return _separated_proxies_groups;
  }

  bool
  proxies_group_separated ( const QString & group_id_n ) const;

  void
  set_proxies_group_separated ( const QString & group_id_n, bool separated_n );

  // -- Proxies group filtering

  const QSet< QString > &
  blacklisted_proxies_groups () const
  {
    return _blacklisted_proxies_groups;
  }

  bool
  proxies_group_blacklisted ( const QString & group_id_n ) const;

  void
  set_proxies_group_blacklisted ( const QString & group_id_n,
                                  bool blacklisted_n );

  private:
  // -- Attributes
  QSet< QString > _separated_proxies_groups;
  QSet< QString > _blacklisted_proxies_groups;
};

} // namespace MState::Mixer
