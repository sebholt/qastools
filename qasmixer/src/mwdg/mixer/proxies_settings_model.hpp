/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QAbstractItemModel>
#include <QBrush>
#include <vector>

// Forward declaration
namespace MWdg::Mixer
{
class Proxies_Model;
}
namespace MWdg::Mixer::Proxy
{
class Group;
}
namespace Wdg
{
class Style_Db;
}

namespace MWdg::Mixer
{

/// @brief Proxies_Settings_Model
///
class Proxies_Settings_Model : public QAbstractItemModel
{
  Q_OBJECT

  public:
  // -- Public types

  enum Root_Entry_Row
  {
    RER_SLIDERS = 0,
    RER_SWITCHES = 1
  };

  static constexpr int num_root_rows = 2;

  enum Index_Id
  {
    IID_INVALID,
    IID_L0_SLIDERS,
    IID_L0_SWITCHES,
    IID_L1_SLIDER,
    IID_L1_SWITCH
  };

  enum Column_Index
  {
    CI_NAME = 0,
    CI_SHOW = 1
  };

  static constexpr int num_data_cols = 2;

  // -- Construction

  Proxies_Settings_Model ( Wdg::Style_Db * wdg_style_db_n,
                           QObject * parent_n = nullptr );

  ~Proxies_Settings_Model () override;

  // -- Setup

  void
  setup ( Proxies_Model * sliders_model_n, Proxies_Model * switches_model_n );

  Proxies_Model *
  sliders_model () const
  {
    return _sliders.model;
  }

  Proxies_Model *
  switches_model () const
  {
    return _switches.model;
  }

  bool
  proxies_models_valid () const
  {
    return ( _sliders.model != nullptr ) && ( _switches.model != nullptr );
  }

  // -- Model interface

  QModelIndex
  index_l0_sliders () const;

  QModelIndex
  index_l0_switches () const;

  QModelIndex
  index ( int row_n,
          int column_n,
          const QModelIndex & parent_n = QModelIndex () ) const override;

  QModelIndex
  parent ( const QModelIndex & index_n ) const override;

  int
  rowCount ( const QModelIndex & parent_n = QModelIndex () ) const override;

  int
  columnCount ( const QModelIndex & parent_n = QModelIndex () ) const override;

  QVariant
  headerData ( int section_n,
               Qt::Orientation orientation_n,
               int role_n = Qt::DisplayRole ) const override;

  Qt::ItemFlags
  flags ( const QModelIndex & index_n ) const override;

  QVariant
  data ( const QModelIndex & index_n,
         int role_n = Qt::DisplayRole ) const override;

  bool
  setData ( const QModelIndex & index_n,
            const QVariant & value_n,
            int role_n = Qt::EditRole ) override;

  // -- Signals

  Q_SIGNAL
  void
  sig_visibility_changed ();

  private:
  // -- Utility

  bool
  valid_sliders_row ( int row_n ) const;

  bool
  valid_switches_row ( int row_n ) const;

  bool
  is_l0_sliders ( const QModelIndex & index_n ) const;

  bool
  is_l1_slider ( const QModelIndex & index_n ) const;

  bool
  is_l0_switches ( const QModelIndex & index_n ) const;

  bool
  is_l1_switch ( const QModelIndex & index_n ) const;

  QBrush
  foreground_brush ( unsigned int style_id_n ) const;

  Q_SLOT
  void
  rebuild_sliders_begin ();

  Q_SLOT
  void
  rebuild_sliders_end ();

  Q_SLOT
  void
  rebuild_switches_begin ();

  Q_SLOT
  void
  rebuild_switches_end ();

  public:
  // -- Attributes
  Wdg::Style_Db * _wdg_style_db = nullptr;
  struct
  {
    Proxies_Model * model = nullptr;
    const std::vector< MWdg::Mixer::Proxy::Group * > * proxies = nullptr;
  } _sliders;
  struct
  {
    Proxies_Model * model = nullptr;
    const std::vector< MWdg::Mixer::Proxy::Group * > * proxies = nullptr;
  } _switches;
};

} // namespace MWdg::Mixer
