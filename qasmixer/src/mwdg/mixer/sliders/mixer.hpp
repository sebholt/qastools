/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "mwdg/mixer/proxy_identifier.hpp"
#include <QAction>
#include <QMenu>
#include <QPointer>
#include <QString>
#include <memory>
#include <vector>

// Forward declaration
namespace MWdg
{
class Inputs_Setup;
} // namespace MWdg
namespace MWdg::Mixer
{
class Setup;
} // namespace MWdg::Mixer
namespace MWdg::Mixer::Sliders
{
class Proxies_Model;
}
namespace MWdg::Mixer::Proxy
{
class Group;
}
namespace Wdg
{
class Scroll_Area_Horizontal;
class Style_Db;
} // namespace Wdg
namespace Wdg::Pad_Proxy
{
class Group;
}
namespace Wdg::Sliders_Pad
{
class Pad;
}
namespace dpe
{
class Image_Allocator;
}

namespace MWdg::Mixer::Sliders
{

/// @brief Mixer
///
class Mixer : public QWidget
{
  Q_OBJECT

  public:
  // -- Construction

  Mixer ( const Wdg::Style_Db * wdg_style_db_n,
          dpe::Image_Allocator * image_alloc_n,
          QWidget * parent_n = nullptr );

  ~Mixer ();

  // -- Mixer setup

  const MWdg::Mixer::Setup *
  mixer_setup () const
  {
    return _mixer_setup;
  }

  void
  set_mixer_setup ( const MWdg::Mixer::Setup * setup_n );

  // -- Proxies model

  MWdg::Mixer::Sliders::Proxies_Model *
  proxies_model () const
  {
    return _proxies_model;
  }

  void
  set_proxies_model ( MWdg::Mixer::Sliders::Proxies_Model * model_n );

  // -- Inputs setup

  const MWdg::Inputs_Setup *
  inputs_setup () const
  {
    return _inputs_setup;
  }

  void
  set_inputs_setup ( const MWdg::Inputs_Setup * setup_n );

  // -- Sliders pad

  Wdg::Sliders_Pad::Pad *
  sliders_pad ()
  {
    return _sliders_pad.get ();
  }

  // -- Signals

  Q_SIGNAL
  void
  sig_footer_label_selected ( unsigned int group_idx_n,
                              unsigned int column_idx_n );

  protected:
  // -- Event processing

  bool
  eventFilter ( QObject * watched, QEvent * event ) override;

  private:
  // -- Utility

  Q_SLOT
  void
  reload_proxies_groups_begin ();

  Q_SLOT
  void
  reload_proxies_groups_end ();

  // -- Action callbacks

  Q_SLOT
  void
  action_level_volumes ();

  Q_SLOT
  void
  action_toggle_joined ();

  Q_SLOT
  void
  action_toggle_mute ();

  Q_SLOT
  void
  update_focus_proxies ();

  // -- Gui state

  void
  gui_state_store ();

  void
  gui_state_restore ();

  MWdg::Mixer::Proxy::Group *
  find_visible_proxy ( const MWdg::Mixer::Proxy_Identifier & proxy_id_n );

  // -- Context menu

  bool
  context_menu_start ( const QPoint & pos_n );

  /// @return The number of visible actions
  std::size_t
  context_menu_update ();

  Q_SLOT
  void
  context_menu_update_visible ();

  private:
  // -- Attributes
  const MWdg::Mixer::Setup * _mixer_setup = nullptr;
  MWdg::Mixer::Sliders::Proxies_Model * _proxies_model = nullptr;
  const MWdg::Inputs_Setup * _inputs_setup = nullptr;

  std::vector< Wdg::Pad_Proxy::Group * > _proxies_groups_pass;

  Wdg::Scroll_Area_Horizontal * _sliders_area;
  std::unique_ptr< Wdg::Sliders_Pad::Pad > _sliders_pad;

  // Action focus proxy
  QPointer< MWdg::Mixer::Proxy::Group > _act_proxies_group;
  unsigned int _act_proxies_column = 0;

  // GUI state
  MWdg::Mixer::Proxy_Identifier _proxy_id;

  // Context menu
  QMenu _cmenu;
  QAction _act_toggle_joined;
  QAction _act_level_channels;
  QAction _act_separator_channels;
  QAction _act_toggle_mute;

  // Strings and Icons
  QString _act_str_mute[ 2 ];
  QString _act_str_unmute[ 2 ];
  QString _act_str_toggle_mute;

  QIcon _icon_vol_high;
  QIcon _icon_vol_med;
  QIcon _icon_muted;
};

} // namespace MWdg::Mixer::Sliders
