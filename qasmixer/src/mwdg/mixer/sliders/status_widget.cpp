/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "status_widget.hpp"
#include "mwdg/mixer/sliders/proxy/slider.hpp"
#include "wdg/pad_proxy/column.hpp"
#include "wdg/pad_proxy/group.hpp"
#include "wdg/sliders_pad/pad.hpp"
#include <iostream>

namespace MWdg::Mixer::Sliders
{

Status_Widget::Status_Widget ( QWidget * parent_n )
: Slider_Status_Widget ( parent_n )
{
}

Status_Widget::~Status_Widget () = default;

void
Status_Widget::select_slider ( unsigned int grp_idx_n, unsigned int col_idx_n )
{
  if ( sliders_pad () == nullptr ) {
    return;
  }
  MWdg::Mixer::Sliders::Proxy::Slider * proxy_new = _proxy_slider;
  if ( grp_idx_n < sliders_pad ()->num_proxies_groups () ) {
    Wdg::Pad_Proxy::Group * pgrp = sliders_pad ()->proxies_group ( grp_idx_n );
    if ( col_idx_n < pgrp->num_sliders () ) {
      proxy_new = dynamic_cast< MWdg::Mixer::Sliders::Proxy::Slider * > (
          pgrp->column ( col_idx_n )->slider_proxy () );
    }
  }
  set_slider_proxy ( proxy_new );
}

void
Status_Widget::proxy_destroyed ()
{
  set_slider_proxy ( nullptr );
}

void
Status_Widget::set_slider_proxy (
    MWdg::Mixer::Sliders::Proxy::Slider * proxy_n )
{
  // std::cout << "Status_Widget::set_slider_proxy " << proxy_n
  //<< "\n";

  if ( _proxy_slider != proxy_n ) {

    // Disconnect previous proxy
    if ( _proxy_slider != nullptr ) {
      if ( _proxy_slider->qsnd_mixer_elem () != nullptr ) {
        disconnect ( _proxy_slider->qsnd_mixer_elem (), 0, this, 0 );
      }
      disconnect ( _proxy_slider, 0, this, 0 );
    }

    _proxy_slider = proxy_n;

    setup_values ();

    if ( _proxy_slider != nullptr ) {
      if ( _proxy_slider->qsnd_mixer_elem () != nullptr ) {
        connect ( _proxy_slider->qsnd_mixer_elem (),
                  SIGNAL ( sig_values_changed () ),
                  this,
                  SLOT ( update_values () ) );
      }
      connect ( _proxy_slider,
                SIGNAL ( destroyed ( QObject * ) ),
                this,
                SLOT ( proxy_destroyed () ) );

      update_values ();
    }
  }
}

QString
Status_Widget::elem_name () const
{
  QString res;
  if ( _proxy_slider != nullptr ) {
    res = _proxy_slider->group_name ();
    if ( res != _proxy_slider->item_name () ) {
      res += " - ";
      res += _proxy_slider->item_name ();
    }
  }
  return res;
}

bool
Status_Widget::elem_has_volume () const
{
  return ( _proxy_slider != nullptr );
}

bool
Status_Widget::elem_has_dB () const
{
  bool res = false;
  if ( _proxy_slider != nullptr ) {
    res = _proxy_slider->has_dB ();
  }
  return res;
}

long
Status_Widget::elem_volume_value () const
{
  long res = 0;
  if ( _proxy_slider != nullptr ) {
    res = _proxy_slider->volume_value ();
  }
  return res;
}

void
Status_Widget::elem_set_volume ( long value_n ) const
{
  if ( _proxy_slider != nullptr ) {
    _proxy_slider->set_volume_value ( value_n );
  }
}

long
Status_Widget::elem_volume_min () const
{
  long res = 0;
  if ( _proxy_slider != nullptr ) {
    res = _proxy_slider->volume_min ();
  }
  return res;
}

long
Status_Widget::elem_volume_max () const
{
  long res = 0;
  if ( _proxy_slider != nullptr ) {
    res = _proxy_slider->volume_max ();
  }
  return res;
}

long
Status_Widget::elem_dB_value () const
{
  long res = 0;
  if ( _proxy_slider != nullptr ) {
    res = _proxy_slider->dB_value ();
  }
  return res;
}

void
Status_Widget::elem_set_nearest_dB ( long dB_value_n ) const
{
  if ( _proxy_slider != nullptr ) {
    const long vol_near ( _proxy_slider->ask_dB_vol_nearest ( dB_value_n ) );
    _proxy_slider->set_volume_value ( vol_near );
  }
}

long
Status_Widget::elem_dB_min () const
{
  long res = 0;
  if ( _proxy_slider != nullptr ) {
    res = _proxy_slider->dB_min ();
  }
  return res;
}

long
Status_Widget::elem_dB_max () const
{
  long res = 0;
  if ( _proxy_slider != nullptr ) {
    res = _proxy_slider->dB_max ();
  }
  return res;
}

} // namespace MWdg::Mixer::Sliders
