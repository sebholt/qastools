/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "mwdg/mixer/proxy/column.hpp"
#include <QLocale>

namespace MWdg::Mixer::Sliders::Proxy
{

// Forward declaration
class Slider;
class Switch;

/// @brief Column
///
class Column : public MWdg::Mixer::Proxy::Column
{
  Q_OBJECT

  public:
  // -- Construction

  Column ();

  ~Column () override;

  void
  update_mixer_values () override;

  // -- Value string

  QString
  value_string () const override;

  QString
  value_min_string () const override;

  QString
  value_max_string () const override;

  protected:
  // -- Event processing

  bool
  event ( QEvent * event_n ) override;

  // -- Utility

  void
  slider_proxy_changed () override;

  void
  switch_proxy_changed () override;

  void
  show_value_string_changed () override;

  void
  update_connections ();

  void
  dB_string ( QString & str_n, long dB_value_n ) const;

  void
  percent_string ( QString & str_n, int permille_n ) const;

  private:
  // -- Proxies

  Slider *
  mixer_slider_proxy () const;

  Switch *
  mixer_switch_proxy () const;

  private:
  // -- Attributes
  QString _str_value_dB;
  QString _str_value_pc;
  QLocale _loc;
};

} // namespace MWdg::Mixer::Sliders::Proxy
