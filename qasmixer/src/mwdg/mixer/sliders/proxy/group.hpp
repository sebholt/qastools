/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "mwdg/mixer/proxy/group.hpp"

namespace MWdg::Mixer::Sliders::Proxy
{

/// @brief Group
///
class Group : public MWdg::Mixer::Proxy::Group
{
  Q_OBJECT

  public:
  // -- Construction

  Group ( QObject * parent_n = nullptr );

  ~Group ();

  // -- Separation info

  bool
  should_be_separated () const override;

  protected:
  // -- Event processing

  bool
  event ( QEvent * event_n ) override;
};

} // namespace MWdg::Mixer::Sliders::Proxy
