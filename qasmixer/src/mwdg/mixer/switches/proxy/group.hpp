/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "mwdg/mixer/proxy/group.hpp"

namespace MWdg::Mixer::Switches::Proxy
{

/// @brief Group
///
class Group : public MWdg::Mixer::Proxy::Group
{
  Q_OBJECT

  public:
  // -- Construction

  Group ( QObject * parent_n = nullptr );

  ~Group ();

  // -- Separation info

  bool
  should_be_separated () const override;
};

} // namespace MWdg::Mixer::Switches::Proxy
