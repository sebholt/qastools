/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "qsnd/mixer/elem.hpp"
#include "wdg/pad_proxy/enum.hpp"

namespace MWdg::Mixer::Switches::Proxy
{

/// @brief Enum
///
class Enum : public Wdg::Pad_Proxy::Enum
{
  Q_OBJECT

  public:
  // -- Construction

  Enum ();

  ~Enum ();

  // -- Mixer element

  QSnd::Mixer::Elem *
  qsnd_mixer_elem () const
  {
    return _qsnd_mixer_elem;
  }

  void
  set_qsnd_mixer_elem ( QSnd::Mixer::Elem * selem_n );

  // -- Snd dir

  unsigned char
  snd_dir () const
  {
    return _snd_dir;
  }

  void
  set_snd_dir ( unsigned char dir_n );

  // -- Channel index

  unsigned int
  channel_idx () const
  {
    return _channel_idx;
  }

  void
  set_channel_idx ( unsigned int idx_n );

  // -- Is joined flag

  bool
  is_joined () const
  {
    return _is_joined;
  }

  void
  set_is_joined ( bool flag_n );

  QString
  enum_item_name ( int idx_n );

  // -- Public slots

  Q_SLOT
  void
  update_value_from_source ();

  protected:
  // -- Protected methods

  void
  enum_index_changed ();

  private:
  // -- Attributes
  QSnd::Mixer::Elem * _qsnd_mixer_elem;

  unsigned int _channel_idx;
  unsigned char _snd_dir;
  bool _is_joined;
  bool _alsa_updating;
};

} // namespace MWdg::Mixer::Switches::Proxy
