/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "column.hpp"
#include "wdg/pad_proxy/enum.hpp"
#include "wdg/pad_proxy/switch.hpp"
#include <iostream>

namespace MWdg::Mixer::Switches::Proxy
{

Column::Column () = default;

Column::~Column () = default;

void
Column::update_mixer_values ()
{
  if ( enum_proxy () != nullptr ) {
    enum_proxy ()->update_value_from_source ();
  }
  if ( switch_proxy () != nullptr ) {
    switch_proxy ()->update_value_from_source ();
  }
}

} // namespace MWdg::Mixer::Switches::Proxy
