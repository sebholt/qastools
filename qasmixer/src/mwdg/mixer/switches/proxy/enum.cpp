/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "enum.hpp"
#include <iostream>

namespace MWdg::Mixer::Switches::Proxy
{

Enum::Enum ()
: _qsnd_mixer_elem ( nullptr )
, _channel_idx ( 0 )
, _snd_dir ( 0 )
, _is_joined ( false )
, _alsa_updating ( false )
{
}

Enum::~Enum () = default;

void
Enum::set_snd_dir ( unsigned char dir_n )
{
  _snd_dir = dir_n;
}

void
Enum::set_channel_idx ( unsigned int idx_n )
{
  _channel_idx = idx_n;
}

void
Enum::set_qsnd_mixer_elem ( QSnd::Mixer::Elem * selem_n )
{
  _qsnd_mixer_elem = selem_n;
}

void
Enum::set_is_joined ( bool flag_n )
{
  _is_joined = flag_n;
}

QString
Enum::enum_item_name ( int idx_n )
{
  QString res;
  if ( qsnd_mixer_elem () != 0 ) {
    if ( static_cast< unsigned int > ( idx_n ) <
         qsnd_mixer_elem ()->enum_item_names ().size () ) {
      res = qsnd_mixer_elem ()->enum_item_names ()[ idx_n ];
    }
  }
  return res;
}

void
Enum::enum_index_changed ()
{
  if ( ( qsnd_mixer_elem () != 0 ) && ( !_alsa_updating ) ) {
    // std::cout << "Enum::enum_index_changed " <<
    //  enum_index() << "\n";

    if ( is_joined () ) {
      qsnd_mixer_elem ()->set_enum_index_all ( snd_dir (), enum_index () );
    } else {
      qsnd_mixer_elem ()->set_enum_index (
          snd_dir (), channel_idx (), enum_index () );
    }

    // std::cout << "Enum::enum_index_changed " << "done"
    //<< "\n";
  }
}

void
Enum::update_value_from_source ()
{
  if ( ( qsnd_mixer_elem () != 0 ) && ( !_alsa_updating ) ) {
    _alsa_updating = true;
    set_enum_index (
        qsnd_mixer_elem ()->enum_index ( snd_dir (), channel_idx () ) );
    _alsa_updating = false;
  }
}

} // namespace MWdg::Mixer::Switches::Proxy
