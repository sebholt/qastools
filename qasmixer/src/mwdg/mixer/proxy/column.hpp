/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "wdg/pad_proxy/column.hpp"

namespace MWdg::Mixer::Proxy
{

/// @brief Column
///
class Column : public Wdg::Pad_Proxy::Column
{
  Q_OBJECT

  public:
  // -- Construction

  Column ();

  ~Column () override;

  virtual void
  update_mixer_values () = 0;
};

} // namespace MWdg::Mixer::Proxy
