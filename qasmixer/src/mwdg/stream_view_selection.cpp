/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "stream_view_selection.hpp"
#include "mwdg/inputs_setup.hpp"
#include "qastools_config.hpp"
#include <QFileInfo>
#include <QHBoxLayout>

namespace MWdg
{

Stream_View_Selection::Stream_View_Selection ( QWidget * parent_n )
: QWidget ( parent_n )
{
  // Button texts
  _act_stream_text[ 0 ] = tr ( "Show playback" );
  _act_stream_text[ 1 ] = tr ( "Show capture" );

  // Button tools tips
  _act_stream_ttip[ 0 ] = tr ( "Show playback controls" );
  _act_stream_ttip[ 1 ] = tr ( "Show capture controls" );

  // Button icons
  {
    const QString fd_app[ 2 ] = { "show-playback", "show-capture" };
    const QString fd_def[ 2 ] = { "media-playback-start", "media-record" };

    QString icon_path_base ( INSTALL_DIR_APP_ICONS );
    icon_path_base.append ( "/" );
    for ( std::size_t ii = 0; ii < 2; ++ii ) {
      QIcon icon;
      {
        QString icon_path ( icon_path_base );
        icon_path.append ( fd_app[ ii ] );
        icon_path.append ( ".svg" );
        QFileInfo finfo ( icon_path );
        if ( finfo.exists () && finfo.isReadable () ) {
          icon = QIcon ( icon_path );
        } else {
          icon = QIcon::fromTheme ( fd_def[ ii ] );
        }
      }
      _act_stream_icon[ ii ] = icon;
    }
  }

  // Buttons
  for ( std::size_t ii = 0; ii < 2; ++ii ) {
    _button[ ii ] = new QPushButton ( this );
    _button[ ii ]->setCheckable ( true );
    if ( _act_stream_icon[ ii ].isNull () ) {
      _button[ ii ]->setText ( _act_stream_text[ ii ] );
    } else {
      _button[ ii ]->setIcon ( _act_stream_icon[ ii ] );
    }
    _button[ ii ]->setToolTip ( _act_stream_ttip[ ii ] );
  }

  connect ( _button[ 0 ],
            &QPushButton::toggled,
            this,
            &Stream_View_Selection::playback_changed );

  connect ( _button[ 1 ],
            &QPushButton::toggled,
            this,
            &Stream_View_Selection::capture_changed );

  // Layout
  {
    QHBoxLayout * lay_hbox = new QHBoxLayout;
    lay_hbox->setContentsMargins ( 0, 0, 0, 0 );
    lay_hbox->addWidget ( _button[ 0 ] );
    lay_hbox->addWidget ( _button[ 1 ] );
    setLayout ( lay_hbox );
  }
}

Stream_View_Selection::~Stream_View_Selection () = default;

void
Stream_View_Selection::set_inputs_setup ( const MWdg::Inputs_Setup * setup_n )
{
  if ( setup_n != nullptr ) {
    // Append key sequence texts to the tooltip
    const QString & mask ( "%1 (%2)" );
    for ( std::size_t ii = 0; ii < 2; ++ii ) {
      const QKeySequence & kseq = setup_n->ks_toggle_vis_stream[ ii ];
      const QString & kstr = kseq.toString ( QKeySequence::NativeText );
      if ( !kstr.isEmpty () ) {
        _button[ ii ]->setToolTip ( mask.arg ( _act_stream_ttip[ ii ], kstr ) );
      }
    }
  }
}

void
Stream_View_Selection::set_playback_silent ( bool value_n )
{
  ++_set_silent[ 0 ];
  _button[ 0 ]->setChecked ( value_n );
  --_set_silent[ 0 ];
}

void
Stream_View_Selection::set_capture_silent ( bool value_n )
{
  ++_set_silent[ 1 ];
  _button[ 1 ]->setChecked ( value_n );
  --_set_silent[ 1 ];
}

void
Stream_View_Selection::set_available ( bool value_n )
{
  if ( _available != value_n ) {
    _available = value_n;
    _button[ 0 ]->setEnabled ( _available );
    _button[ 1 ]->setEnabled ( _available );
  }
}

void
Stream_View_Selection::playback_changed ( bool value_n )
{
  if ( _set_silent[ 0 ] == 0 ) {
    Q_EMIT sig_playback ( value_n );
  }
}

void
Stream_View_Selection::capture_changed ( bool value_n )
{
  if ( _set_silent[ 1 ] == 0 ) {
    Q_EMIT sig_capture ( value_n );
  }
}

} // namespace MWdg
