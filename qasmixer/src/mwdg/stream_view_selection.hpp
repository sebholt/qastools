/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QIcon>
#include <QPushButton>
#include <QString>
#include <QWidget>

namespace MWdg
{

// Forward declarations
class Inputs_Setup;

/// @brief Stream_View_Selection
///
class Stream_View_Selection : public QWidget
{
  Q_OBJECT

  public:
  // -- Construction

  Stream_View_Selection ( QWidget * parent_n = nullptr );

  ~Stream_View_Selection ();

  // -- Setup

  void
  set_inputs_setup ( const MWdg::Inputs_Setup * setup_n );

  // -- Playback

  Q_SLOT
  void
  set_playback_silent ( bool value_n );

  Q_SIGNAL
  void
  sig_playback ( bool value_n );

  // -- Capture

  Q_SLOT
  void
  set_capture_silent ( bool value_n );

  Q_SIGNAL
  void
  sig_capture ( bool value_n );

  // -- Available

  bool
  available () const
  {
    return _available;
  }

  Q_SLOT
  void
  set_available ( bool value_n );

  private:
  // -- Private slots

  Q_SLOT
  void
  playback_changed ( bool value_n );

  Q_SLOT
  void
  capture_changed ( bool value_n );

  private:
  // -- Attributes
  // Strings and Icons
  QString _act_stream_text[ 2 ];
  QString _act_stream_ttip[ 2 ];
  QIcon _act_stream_icon[ 2 ];
  int _set_silent[ 2 ] = { 0, 0 };
  bool _available = true;
  QPushButton * _button[ 2 ];
};

} // namespace MWdg
