/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QAction>
#include <QObject>
#include <QString>
#include <array>
#include <memory>

namespace MWdg
{

// Forward declarations
class Inputs_Setup;

/// @brief Stream_View_Selection
///
class Stream_View_Selection_Actions : public QObject
{
  Q_OBJECT

  public:
  // -- Construction

  Stream_View_Selection_Actions ( QObject * parent_n = nullptr );

  ~Stream_View_Selection_Actions ();

  // -- Setup

  void
  set_inputs_setup ( const MWdg::Inputs_Setup * setup_n );

  // -- Playback

  QAction *
  act_playback () const
  {
    return _actions[ 0 ].get ();
  }

  Q_SLOT
  void
  set_playback_silent ( bool value_n );

  Q_SIGNAL
  void
  sig_playback ( bool value_n );

  // -- Capture

  QAction *
  act_capture () const
  {
    return _actions[ 1 ].get ();
  }

  Q_SLOT
  void
  set_capture_silent ( bool value_n );

  Q_SIGNAL
  void
  sig_capture ( bool value_n );

  // -- Available

  bool
  available () const
  {
    return _available;
  }

  Q_SLOT
  void
  set_available ( bool value_n );

  private:
  // -- Private slots

  Q_SLOT
  void
  playback_changed ( bool value_n );

  Q_SLOT
  void
  capture_changed ( bool value_n );

  private:
  // -- Attributes
  // Strings and Icons
  QString _act_stream_text[ 2 ];
  QIcon _act_stream_icon[ 2 ];
  int _set_silent[ 2 ] = { 0, 0 };
  bool _available = true;
  std::array< std::unique_ptr< QAction >, 2 > _actions;
};

} // namespace MWdg
