# About

QasTools is a collection of Qt-based mixer and setup tools for the
Linux sound system ALSA.  At the moment there are three applications:

- **QasMixer** - A graphical mixer similiar to alsamixer
- **QasHctl** - A graphical mixer for ALSA's "High level Control Interface"
- **QasConfig** - A viewer for ALSA's configuration tree 


## Installation

QasTools uses the CMake build system.

For a system wide installation type:

```
git clone https://gitlab.com/sebholt/qastools.git
cd qastools
mkdir build
cd build
cmake ..
make -j8
sudo make install
```

For a local build instead call:

```
git clone https://gitlab.com/sebholt/qastools.git
cd qastools
mkdir install
mkdir build
cd build
cmake .. -DCMAKE_INSTALL_PREFIX=../install
make -j8
make install
```


## Localization

QasTools supports localization (l10n).  If you like to update or add
a translation, see the Localization.md file that comes with this package.


## Copying

QasTools is distributed under the terms in the COPYING text file that
comes with this package.


## Authors

QasTools was written by:

Sebastian Holtermann <sebholt@web.de>
