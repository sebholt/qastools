/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "qsnd/alsa.hpp"
#include "static_tree_model.hpp"

namespace QSnd
{

/// @brief Alsa_Config_Model
///
class Alsa_Config_Model : public Static_Tree_Model
{
  Q_OBJECT

  public:
  // -- Construction

  Alsa_Config_Model ();

  ~Alsa_Config_Model ();

  // -- Model methods

  QVariant
  headerData ( int section,
               Qt::Orientation orientation,
               int role = Qt::DisplayRole ) const override;

  Qt::ItemFlags
  flags ( const QModelIndex & index_n ) const override;

  QVariant
  data ( const QModelIndex & index_n,
         int role = Qt::DisplayRole ) const override;

  QString
  index_address_str ( const QModelIndex & index_n );

  QModelIndex
  index_from_address ( const QString & addr_n );

  // Public slots
  public:
  Q_SLOT
  int
  reload ();

  private:
  // -- Private methods

  void
  clear_config ();

  int
  load_config ();

  int
  add_children_recursively ( std::size_t node_idx_n, snd_config_t * cfg_n );

  snd_config_t *
  cfg_struct ( const Node * node_n ) const;

  snd_config_t *
  cfg_struct ( const QModelIndex & index_n ) const;

  std::size_t
  cfg_count_children ( snd_config_t * cfg_n ) const;

  snd_config_t *
  cfg_child ( snd_config_t * cfg_n, std::size_t index_n ) const;

  QString
  cfg_id_string ( snd_config_t * cfg_n ) const;

  QString
  cfg_value_string ( snd_config_t * cfg_n ) const;

  private:
  // -- Attributes
  snd_config_t * _snd_cfg_root = nullptr;
};

} // namespace QSnd
