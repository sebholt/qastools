/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "dpe/image_allocator.hpp"
#include "main_window_setup.hpp"
#include "mwdg/mixer_style_db.hpp"
#include "qsnd/cards_db.hpp"
#include "qsnd/ctl_address.hpp"
#include "views/mixer_hctl.hpp"
#include <QDialog>
#include <QFileSystemWatcher>
#include <QMainWindow>
#include <QPointer>
#include <QSplitter>
#include <memory>

// Forward declaration
namespace Views
{
class Mixer_HCTL;
}
namespace Views
{
class Device_Selection_View;
}

/// @brief Main_Window
///
class Main_Window : public QMainWindow
{
  Q_OBJECT

  public:
  // -- Construction

  Main_Window ( QWidget * parent_n = nullptr,
                Qt::WindowFlags flags_n = Qt::WindowFlags () );

  ~Main_Window ();

  // -- Size hints

  QSize
  sizeHint () const override;

  // -- State store and restore

  void
  restore_state ();

  void
  save_state ();

  // -- Public slots

  Q_SLOT
  void
  select_snd_ctl ( const QSnd::Ctl_Address & ctl_n );

  Q_SLOT
  void
  reload_mixer_device ();

  Q_SLOT
  void
  refresh ();

  Q_SLOT
  void
  set_fullscreen ( bool flag_n );

  // Device selection

  Q_SLOT
  void
  show_device_selection ( bool flag_n );

  Q_SLOT
  void
  toggle_device_selection ();

  protected:
  // -- Protected methods

  void
  changeEvent ( QEvent * event_n ) override;

  void
  closeEvent ( QCloseEvent * event_n ) override;

  private:
  // -- Private slots

  Q_SLOT
  void
  select_ctl_from_side_iface ();

  Q_SLOT
  void
  show_dialog_about ();

  Q_SLOT
  void
  show_dialog_about_qt ();

  // -- Private methods

  void
  update_fullscreen_action ();

  private:
  // -- Attributes
  // Shared storages and settings
  MWdg::Mixer_Style_Db _mixer_style_db;
  dpe::Image_Allocator _image_alloc;
  QSnd::Cards_Db _cards_db;

  // Widgets settings
  Main_Window_Setup _setup;

  // Widgets
  std::unique_ptr< QSplitter > _splitter;
  Views::Mixer_HCTL * _mixer_hctl;
  Views::Device_Selection_View * _dev_select;

  QPointer< QDialog > _info_dialog;

  // Menubar
  QMenu * _menu_mixer;
  QAction * _act_show_dev_select;
  QAction * _act_fullscreen;

  // Strings and icons
  QString _str_fscreen_enable;
  QString _str_fscreen_disable;
  QIcon _icon_fscreen_enable;
  QIcon _icon_fscreen_disable;
};
