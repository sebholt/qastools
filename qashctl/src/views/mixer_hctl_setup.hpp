/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "views/view_base_setup.hpp"
#include <QString>

namespace Views
{

/// @brief Mixer_HCTL_Setup
///
class Mixer_HCTL_Setup : public Views::View_Base_Setup
{
  public:
  // -- Construction

  Mixer_HCTL_Setup ();

  ~Mixer_HCTL_Setup () override;

  public:
  // -- Attributes
  QByteArray splitter_state;
  QString iface_name;
  QString elem_grp_name;
  unsigned int elem_grp_index = 9999;
};

} // namespace Views
