/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "wdg/label_width.hpp"
#include <QAction>
#include <QLabel>
#include <QMenu>
#include <QString>
#include <memory>

namespace QSnd::HCtl
{
class Elem_Group;
class Info_Db;
} // namespace QSnd::HCtl
namespace dpe
{
class Image_Allocator;
}
namespace Wdg
{
class Style_Db;
}
namespace MWdg::HCtl
{
class Editor;
class Editor_Data;
} // namespace MWdg::HCtl
namespace MWdg
{
class Inputs_Setup;
} // namespace MWdg

namespace MWdg::HCtl
{

/// @brief Mixer
///
/// This widgets shows the appropriate MWdg::HCtl::Editor widget
/// for a given QSnd::HCtl::Elem_Group
///
class Mixer : public QWidget
{
  Q_OBJECT

  // Public typedefs
  public:
  struct Label_Pair
  {
    Wdg::Label_Width name;
    Wdg::Label_Width value;
  };

  public:
  // -- Construction

  Mixer ( Wdg::Style_Db * wdg_style_db_n,
          dpe::Image_Allocator * image_alloc_n,
          QWidget * parent_n = nullptr );

  ~Mixer ();

  // -- Shared data

  void
  set_ctl_info_db ( const QSnd::HCtl::Info_Db * info_db_n );

  const QSnd::HCtl::Info_Db *
  ctl_info_db () const;

  // -- Element

  QSnd::HCtl::Elem_Group *
  snd_elem_group () const;

  unsigned int
  elem_idx () const;

  void
  set_snd_elem_group ( QSnd::HCtl::Elem_Group * elem_group_n,
                       unsigned int index_n = 0 );

  // -- Image allocator

  dpe::Image_Allocator *
  image_alloc () const;

  // -- Widget style db

  const Wdg::Style_Db *
  wdg_style_db () const;

  // -- Inputs setup

  const MWdg::Inputs_Setup *
  inputs_setup () const
  {
    return _inputs_setup;
  }

  void
  set_inputs_setup ( const MWdg::Inputs_Setup * setup_n );

  protected:
  // -- Protected methods

  void
  clear ();

  void
  update_info ();

  void
  setup_widgets ();

  private:
  // -- Attributes
  std::unique_ptr< MWdg::HCtl::Editor_Data > _editor_data;
  const MWdg::Inputs_Setup * _inputs_setup;

  // Widgets
  QFrame _info_wdg;
  QWidget _pad_wdg;

  std::unique_ptr< MWdg::HCtl::Editor > _editor_pad;

  QLabel _info_lbl_name;
  Label_Pair _info_lbl_index;
  Label_Pair _info_lbl_dev;
  Label_Pair _info_lbl_flags;
  Label_Pair _info_lbl_count;
  Label_Pair _info_lbl_numid;

  QString _info_dev_mask;
  QString _ttip_name_lbl_mask;
};

} // namespace MWdg::HCtl
