/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "mwdg/hctl/proxy/proxy.hpp"
#include <QObject>

namespace MWdg::HCtl::Proxy
{

/// @brief Enum
///
class Enum : public MWdg::HCtl::Proxy::Proxy
{
  Q_OBJECT

  public:
  // -- Construction

  Enum ( QObject * parent_n );

  ~Enum ();

  // -- Enum

  unsigned int
  enum_index () const
  {
    return _enum_index;
  }

  unsigned int
  enum_num_items () const;

  const char *
  enum_item_name ( unsigned int index_n );

  // -- Signals

  Q_SIGNAL
  void
  sig_enum_index_changed ( unsigned int index_n );

  Q_SIGNAL
  void
  sig_enum_index_changed ( int index_n );

  // -- Public slots

  Q_SLOT
  void
  set_enum_index ( unsigned int index_n );

  Q_SLOT
  void
  set_enum_index ( int index_n );

  Q_SLOT
  void
  update_value_from_source ();

  protected:
  // -- Protected methods

  void
  enum_index_changed ();

  private:
  // -- Attributes
  unsigned int _enum_index;
  bool _updating_state;
};

} // namespace MWdg::HCtl::Proxy
