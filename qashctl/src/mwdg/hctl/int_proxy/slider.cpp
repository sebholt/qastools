/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "slider.hpp"
#include "qsnd/hctl/elem.hpp"
#include <QApplication>
#include <QFocusEvent>
#include <iostream>

namespace MWdg::HCtl::Int_Proxy
{

Slider::Slider ( QSnd::HCtl::Elem * elem_n, unsigned int elem_idx_n )
: _integer_value ( 0 )
, _is_joined ( false )
, _updating_state ( false )
{
  _snd_elem = elem_n;
  _elem_idx = elem_idx_n;

  assert ( _snd_elem != nullptr );

  set_slider_index_max (
      Wdg::integer_distance ( integer_min (), integer_max () ) );
}

Slider::~Slider () = default;

long
Slider::integer_min () const
{
  return snd_elem ()->integer_min ();
}

long
Slider::integer_max () const
{
  return snd_elem ()->integer_max ();
}

void
Slider::set_joined ( bool flag_n )
{
  _is_joined = flag_n;
}

void
Slider::set_integer_value ( long value_n )
{
  if ( integer_value () != value_n ) {
    _integer_value = value_n;
    this->integer_value_changed ();
    Q_EMIT sig_integer_value_changed ( integer_value () );
    Q_EMIT sig_integer_value_changed ( int ( integer_value () ) );
  }
}

void
Slider::set_integer_value ( int value_n )
{
  set_integer_value ( long ( value_n ) );
}

void
Slider::integer_value_changed ()
{
  // std::cout << "Slider::integer_value_changed " <<
  //  integer_value() << "\n";

  if ( snd_elem () != nullptr ) {
    set_slider_index ( integer_to_index ( integer_value () ) );
  }

  if ( ( snd_elem () != nullptr ) && ( snd_elem ()->is_writable () ) &&
       !_updating_state ) {
    if ( is_joined () || joined_by_key () ) {
      snd_elem ()->set_integer_all ( integer_value () );
    } else {
      snd_elem ()->set_integer ( elem_idx (), integer_value () );
    }
  }
}

void
Slider::slider_index_changed ()
{
  // std::cout << "Slider::slider_index_changed " <<
  //  integer_index() << "\n";

  if ( snd_elem () != nullptr ) {
    set_integer_value ( index_to_integer ( slider_index () ) );
  }
}

void
Slider::update_value_from_source ()
{
  if ( ( snd_elem () != 0 ) && !_updating_state ) {
    _updating_state = true;
    set_integer_value ( snd_elem ()->integer_value ( elem_idx () ) );
    _updating_state = false;
  }
}

bool
Slider::joined_by_key () const
{
  bool res ( true );
  if ( ( QApplication::keyboardModifiers () & Qt::ControlModifier ) == 0 ) {
    res = false;
  }
  res = ( res && has_focus () );
  return res;
}

} // namespace MWdg::HCtl::Int_Proxy
