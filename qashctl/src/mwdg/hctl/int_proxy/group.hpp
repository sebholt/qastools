/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "wdg/pad_proxy/group.hpp"

// Forward declaration
namespace QSnd::HCtl
{
class Elem;
}
namespace MWdg::HCtl::Int_Proxy
{
class Column;
}

namespace MWdg::HCtl::Int_Proxy
{

/// @brief Group
///
class Group : public Wdg::Pad_Proxy::Group
{
  Q_OBJECT

  public:
  // -- Construction

  Group ( QSnd::HCtl::Elem * snd_elem_n, QObject * parent_n );

  ~Group ();

  // -- Notify parent on value change

  bool
  notify_value_change () const
  {
    return _notify_value_change;
  }

  void
  set_notify_value_change ( bool flag_n );

  // -- Status

  bool
  is_joined () const;

  bool
  volumes_equal () const;

  MWdg::HCtl::Int_Proxy::Column *
  mcolumn ( std::size_t idx_n ) const;

  // -- Public slots

  Q_SLOT
  void
  set_joined ( bool flag_n );

  Q_SLOT
  void
  level_volumes ( unsigned int column_n = 0 );

  Q_SLOT
  void
  update_values ();

  private:
  // -- Attributes
  QSnd::HCtl::Elem * _snd_elem;
  bool _notify_value_change;
};

} // namespace MWdg::HCtl::Int_Proxy
