/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "wdg/pad_proxy/slider.hpp"
#include "wdg/uint_mapper.hpp"
#include <QObject>

// Forward declaration
namespace QSnd::HCtl
{
class Elem;
}

namespace MWdg::HCtl::Int_Proxy
{

/// @brief Slider
///
class Slider : public Wdg::Pad_Proxy::Slider
{
  Q_OBJECT

  public:
  // -- Construction

  Slider ( QSnd::HCtl::Elem * elem_n, unsigned int elem_idx_n );

  ~Slider ();

  // -- Snd element

  QSnd::HCtl::Elem *
  snd_elem () const
  {
    return _snd_elem;
  }

  unsigned int
  elem_idx () const
  {
    return _elem_idx;
  }

  // -- Joined

  bool
  is_joined () const
  {
    return _is_joined;
  }

  void
  set_joined ( bool flag_n );

  bool
  joined_by_key () const;

  // -- Integer value

  long
  integer_min () const;

  long
  integer_max () const;

  long
  integer_value () const
  {
    return _integer_value;
  }

  unsigned long
  integer_to_index ( long integer_n ) const
  {
    unsigned long res = Wdg::integer_distance ( integer_min (), integer_n );
    return res;
  }

  long
  index_to_integer ( unsigned long index_n ) const
  {
    long res = integer_min ();
    res += index_n;
    return res;
  }

  // -- Signals

  Q_SIGNAL
  void
  sig_integer_value_changed ( long value_n );

  Q_SIGNAL
  void
  sig_integer_value_changed ( int value_n );

  // -- Public slots

  Q_SLOT
  void
  set_integer_value ( long value_n );

  Q_SLOT
  void
  set_integer_value ( int value_n );

  Q_SLOT
  void
  update_value_from_source ();

  protected:
  // -- Protected methods

  void
  integer_value_changed ();

  void
  slider_index_changed ();

  private:
  // -- Attributes
  QSnd::HCtl::Elem * _snd_elem;
  unsigned int _elem_idx;

  long _integer_value;
  bool _is_joined;
  bool _updating_state;
};

} // namespace MWdg::HCtl::Int_Proxy
