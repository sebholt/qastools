/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "column.hpp"
#include <iostream>

namespace MWdg::HCtl::Int_Proxy
{

Column::Column ()
{
  //: Decibel value string template
  _str_value_dB = tr ( "%1 dB" );
  //: Percent value string template
  _str_value_pc = tr ( "%1 %" );
}

Column::~Column () = default;

void
Column::slider_proxy_changed ()
{
  update_connections ();
}

void
Column::show_value_string_changed ()
{
  update_connections ();
}

void
Column::update_connections ()
{
  if ( slider_proxy () != nullptr ) {
    MWdg::HCtl::Int_Proxy::Slider * msps = mslider_proxy ();
    disconnect ( msps, 0, this, 0 );
    if ( show_value_string () ) {
      connect ( msps,
                SIGNAL ( sig_slider_index_changed ( unsigned long ) ),
                this,
                SIGNAL ( sig_value_string_changed () ) );
    }
  }
}

// Value string

QString
Column::value_string () const
{
  QString res;
  if ( has_slider () && show_value_string () ) {
    MWdg::HCtl::Int_Proxy::Slider * msps = mslider_proxy ();
    integer_string ( res, msps->integer_value () );
  }
  return res;
}

QString
Column::value_min_string () const
{
  QString res;
  if ( has_slider () && show_value_string () ) {
    MWdg::HCtl::Int_Proxy::Slider * msps = mslider_proxy ();
    integer_string ( res, msps->integer_min () );
  }
  return res;
}

QString
Column::value_max_string () const
{
  QString res;
  if ( has_slider () && show_value_string () ) {
    MWdg::HCtl::Int_Proxy::Slider * msps = mslider_proxy ();
    integer_string ( res, msps->integer_max () );
  }
  return res;
}

void
Column::integer_string ( QString & str_n, long value_n ) const
{
  str_n = _loc.toString ( value_n );
}

void
Column::dB_string ( QString & str_n, long dB_value_n ) const
{
  str_n = _loc.toString ( dB_value_n / 100.0, 'f', 2 );
  str_n = _str_value_dB.arg ( str_n );
}

void
Column::percent_string ( QString & str_n, int permille_n ) const
{
  str_n = _loc.toString ( permille_n / 10.0, 'f', 1 );
  str_n = _str_value_pc.arg ( str_n );
}

} // namespace MWdg::HCtl::Int_Proxy
