/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include "mwdg/hctl/editor.hpp"

namespace MWdg::HCtl::Edit
{

/// @brief Unsupported
///
class Unsupported : public MWdg::HCtl::Editor
{
  Q_OBJECT

  public:
  // -- Construction

  Unsupported ( MWdg::HCtl::Editor_Data * data_n,
                QWidget * parent_n = nullptr );

  ~Unsupported ();

  // -- Setup

  void
  setup_single ();

  private:
  // -- Attributes
  QString _str_unsupported;
};

} // namespace MWdg::HCtl::Edit
