/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#include "unsupported.hpp"
#include "mwdg/hctl/editor_data.hpp"
#include "qsnd/hctl/elem.hpp"
#include "qsnd/hctl/elem_group.hpp"
#include <QGridLayout>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <iostream>

namespace MWdg::HCtl::Edit
{

Unsupported::Unsupported ( MWdg::HCtl::Editor_Data * data_n,
                           QWidget * parent_n )
: MWdg::HCtl::Editor ( data_n, parent_n )
{
  _str_unsupported = tr ( "Elements of the type %1 are not supported" );
  setup_single ();
}

Unsupported::~Unsupported () {}

void
Unsupported::setup_single ()
{
  QLabel * lbl_head ( new QLabel );
  {
    QSnd::HCtl::Elem * elem ( editor_data ()->snd_elem_group->elem ( 0 ) );

    QString val ( elem->elem_type_display_name () );
    val = _str_unsupported.arg ( val );
    val = QString ( "<h4>%1</h4>" ).arg ( val );
    lbl_head->setText ( val );
  }

  // Pad layout
  {
    const unsigned int vspace ( qMax ( 0, fontMetrics ().height () / 2 ) );
    QVBoxLayout * lay_pad ( new QVBoxLayout );
    lay_pad->setContentsMargins ( 0, 0, 0, 0 );
    lay_pad->addSpacing ( vspace );
    lay_pad->addWidget ( lbl_head, 0 );
    lay_pad->addStretch ( 1 );
    setLayout ( lay_pad );
  }
}

} // namespace MWdg::HCtl::Edit
