/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QLocale>
#include <QString>

// Forward declaration
namespace QSnd::HCtl
{
class Elem;
class Elem_Group;
class Info_Db;
} // namespace QSnd::HCtl
namespace dpe
{
class Image_Allocator;
}
namespace Wdg
{
class Style_Db;
}

namespace MWdg::HCtl
{

/// @brief Editor_Data
///
class Editor_Data
{
  public:
  // -- Construction

  Editor_Data ();

  ~Editor_Data ();

  public:
  // -- Attributes
  const QSnd::HCtl::Info_Db * ctl_info_db;
  const Wdg::Style_Db * wdg_style_db;

  // Group and element selection
  QSnd::HCtl::Elem_Group * snd_elem_group;
  unsigned int elem_idx;

  dpe::Image_Allocator * image_alloc;

  // Strings
  QLocale loc;
  QString str_joined;
  QString ttip_grid_lbl_elem;
  QString ttip_grid_lbl_channel;
  QString ttip_grid_widget;

  QString str_list_channel;
  QString ttip_list_channel;
};

} // namespace MWdg::HCtl
