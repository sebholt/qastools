/// QasTools: Desktop toolset for the Linux sound system ALSA.
/// \copyright See COPYING file.

#pragma once

#include <QAbstractButton>
#include <QGridLayout>
#include <QLabel>
#include <QLocale>
#include <QPalette>
#include <QWidget>
#include <vector>

// Forward declaration
namespace QSnd::HCtl
{
class Elem;
class Elem_Group;
} // namespace QSnd::HCtl
namespace MWdg::HCtl
{
class Editor_Data;
}
namespace MWdg::HCtl::Proxy
{
class Group;
}
namespace MWdg
{
class Inputs_Setup;
} // namespace MWdg

namespace MWdg::HCtl
{

/// @brief Editor
///
class Editor : public QWidget
{
  Q_OBJECT

  public:
  // -- Construction

  Editor ( MWdg::HCtl::Editor_Data * data_n, QWidget * parent_n = nullptr );

  ~Editor ();

  // -- Editor data

  const MWdg::HCtl::Editor_Data *
  editor_data () const
  {
    return _editor_data;
  }

  MWdg::HCtl::Editor_Data *
  editor_data ()
  {
    return _editor_data;
  }

  // -- Inputs setup

  const MWdg::Inputs_Setup *
  inputs_setup () const
  {
    return _inputs_setup;
  }

  virtual void
  set_inputs_setup ( const MWdg::Inputs_Setup * setup_n );

  virtual void
  update_proxies_values ();

  QWidget *
  create_small_joined_switch ( MWdg::HCtl::Proxy::Group * pgroup_n,
                               QSnd::HCtl::Elem * elem_n );

  protected:
  // -- Protected methods

  unsigned int
  elem_style_id ( const QSnd::HCtl::Elem * elem_n ) const;

  QGridLayout *
  create_channel_grid ( const std::vector< QWidget * > & items_n,
                        bool bold_labels_n = false );

  private:
  // -- Attributes
  MWdg::HCtl::Editor_Data * _editor_data;
  const MWdg::Inputs_Setup * _inputs_setup;
};

} // namespace MWdg::HCtl
