# Localization

There are two aspects in QasTools that support localization:

- metadata
- applications

## Metadata translation

The QasTools metadata sits in the text and xml files that contain strings and paragraphs
for which translations can be added.

A regular text editor can be used to edit the files:

```
qasconfig/share/application.appdata.xml
qasconfig/share/application.desktop
qashctl/share/application.appdata.xml
qashctl/share/application.desktop
qasmixer/share/application.appdata.xml
qasmixer/share/application.desktop
```

## Application translation

The translation for all applications sits in a single `app_xy.ts` file,
where `xy` is a two letter language key.

All app `app_xy.ts` files can be found in the directory `i18n/ts`.


### lupdate - update the `app_xy.ts` file

To generate or update an `app_xy.ts` file for your language from QasTools' sources,
Qt's `lupdate` tool is used in the following way:

```
cd i18n
lupdate @l10n_sources.txt -ts ts/app_xy.ts
```

Note, that the `lupdate` application may not be in the `PATH` but rather can be found in
`/usr/lib/qt6/bin/lupdate` (on a Debian based system), so that the actual call is:

```
cd i18n
/usr/lib/qt6/bin/lupdate @l10n_sources.txt -ts ts/app_xy.ts
```

### linguist - translation

Now that we have the `app_xy.ts` file, we can start the actual translation with Qt's `linguist` tool, which again may be found under `/usr/lib/qt6/bin/linguist`:

```
cd i18n/ts
linguist app_xy.ts
```

If this is a new translation, the `app_xy.ts` file must be registered in `i18n/CMakeLists.txt`.

To test the translation, install it by calling:

```
make install
```

in your local QasTools build and start the QasTools applications to see if the translation is Ok.


## Upstream

If you like your translation to be integrated into the official QasTools package, create a merge request in [gitlab](https://gitlab.com/sebholt/qastools/-/merge_requests).

Alternatively you can send the translated metadata files and the `app_xy.ts` file to [sebholt@web.de](mailto:sebholt@web.de).
